Thanks you for downloading this XNA Lara Model.

Ada RE6 Spy Outfit

Instructions:
To use him, just copy the whole folder inside the data folder which is in the folder of XNA Lara.

Reports:
If you find any bug on it, you can report it on my DeviantArt page (a Note to me or a post on the same deviant of the model).

http://adngel.deviantart.com/

Wihout anymore to say, I hope you enjoy this model. :D


Credits:
Ada Wong is a character of Resident Evil Saga, property of Capcom.

Updates:

20 Jul 2014
---------------
* Re-extracted from the game, with game bones.
* Textures from PC version
* Cinematic head rigging
* Micro-Bump maps applied

* Hair duplicated (you should use the XPS options "Back-face culling" and "Always Force Culling", but if for any reason it's not possible on your scene, you also can show or hide the inside hair faces with the Control+A menu, to avoid the double faces).

* Holster made as optional item. You can show or hide it with the Ctrl+A menu.

Tools:
Mariokart64n's script
Maliweii's script
3ds Max 2011
Mariokart64n's Xnalaraconverter
XPS 10.9.8.7.6.5.4.3.2
Photoshop

06 Oct 2012
---------------
Add some normal mappings done by me, it's to say, bump mapping are not from the original game yet.
Fixed a rigging problem in the wrist.
Rigged the hair as I could >.< .
Change some specular values in clothes, hair and lips. (shines).
Bones renamed.

15 Nov 2012
---------------
Fixed a rigging problem in finger of the right hand.
The head neck lower has been changed to can be used without influence with shoulders.

Model extracted by Soroosh
Model Rigged and ported for XNA Lara by Adngel

