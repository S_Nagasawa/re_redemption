﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using System.Linq;

using Product.Systems;

namespace Product.Character
{
	/// <summary>
	/// Player character.
	/// </summary>
	[RequireComponent(typeof(PlayerController))]
	public class PlayerCharacter : CharacterModel
	{

		private float 	m_OrigGroundCheckDistance;
		private float 	m_TurnAmount;
		private float 	m_ForwardAmount;

		#region CapsuleCollider original
		private Vector3 m_OriginalCapsuleCenter;
		private float 	m_OriginalCapsuleHeight;
		private float   m_OriginalCapsuleRadius;
		#endregion

		#region CrouchingColliderSize
		private Vector3 m_CrouchingCenter = new Vector3 (0.06f, 0.5f, 0f);
		private float   m_CrouchingHeight = 1f;
		private float   m_CrouchingRadius = 0.3f;
		#endregion

		#region Static Animator Keys
		private static readonly string OnCrouch = "Crouch";     //しゃがむ
		private static readonly string OnKick   = "KickAction"; //ける
		#endregion


		#region Override
		protected override void Awake ()
		{
			base.Awake ();
			_CreateStatus ();
		}


		/// <summary>
		/// Create the status.
		/// @TODO 決め打ち
		/// </summary>
		void _CreateStatus ()
		{
			if (base.Status == null)
				base.Status = new CharacterStatus ();
			base.Status.Name    = this.name.ToString ();
			base.Status.Hp      = 2000;
			base.Status.MaxHp   = base.Status.Hp;
			base.Status.Attack  = 500;
			base.Status.Defence = 500;
			base.Status.Weight  = 45f;
		}


		/// <summary>
		/// Start this instance.
		/// </summary>
		protected override void Start ()
		{
			base.Start ();
			m_OriginalCapsuleHeight   = base.CapsuleController.height;
			m_OriginalCapsuleCenter   = base.CapsuleController.center;
			m_OriginalCapsuleRadius   = base.CapsuleController.radius;
			m_OrigGroundCheckDistance = base.GroundCheckDistance;

			base.LifeCycleStream
				.Where(_ => base.Status != null)
				.Subscribe (_ => {
					if (base.Status.IsDie()) 
					{
						base.Kill();	
					}
				}).AddTo (this);
		}
			

		/// <summary>
		/// Move the specified move, crouch and jump.
		/// </summary>
		/// <param name="move">Move.</param>
		/// <param name="crouch">If set to <c>true</c> crouch.</param>
		/// <param name="jump">If set to <c>true</c> jump.</param>
		public override void Move(Vector3 move, bool crouch = false, bool jump = false)
		{
			if (move.magnitude > 1f) 
				move.Normalize();

			move = base.TransformCache.InverseTransformDirection(move);
			base.CheckGroundStatus();
			move = Vector3.ProjectOnPlane(move, base.GroundNormal);
			m_TurnAmount 	= Mathf.Atan2(move.x, move.z);
			m_ForwardAmount = move.z;

			base.ApplyExtraTurnRotation(m_ForwardAmount, m_TurnAmount);

			if (IsGrounded)
			{
				base.HandleGroundedMovement(crouch, jump);
			}
			else
			{
				base.HandleAirborneMovement(m_OrigGroundCheckDistance);
			}
			ScaleCapsuleForCrouching(crouch);
			PreventStandingInLowHeadroom();

			UpdateAnimator(move);
			base.UpdateState (move);
			base.UpdateStateAction (move);
		}
		#endregion


		/// <summary>
		/// Updates the animator.
		/// </summary>
		/// <param name="move">Move.</param>
		protected override void UpdateAnimator(Vector3? move = null)
		{
			var position = move ?? Vector3.zero;
			// update the animator parameters
			base.AnimatorController.SetFloat(CharacterModel.OnForward, m_ForwardAmount, 0.1f, Time.deltaTime);
			base.AnimatorController.SetFloat(CharacterModel.OnTurn, m_TurnAmount, 0.1f, Time.deltaTime);
			base.AnimatorController.SetBool (CharacterModel.OnGround, base.IsGrounded);
			base.AnimatorController.SetBool (PlayerCharacter.OnCrouch, base.IsCrouching);
			if (!base.IsGrounded)
			{
				base.AnimatorController.SetFloat(CharacterModel.OnJump, base.RigidController.velocity.y);
			}
			float runCycle 	= Mathf.Repeat(base.AnimatorController.GetCurrentAnimatorStateInfo(0).normalizedTime + RunCycleLegOffset, 1);
			float jumpLeg 	= (runCycle < base.CircleHalf ? 1 : -1) * m_ForwardAmount;

			if (base.IsGrounded)
			{
				base.AnimatorController.SetFloat(CharacterModel.OnJumpLeg, jumpLeg);
			}
			if (base.IsGrounded && position.magnitude > 0)
			{
				base.AnimatorController.speed = base.AnimSpeedMultiplier;
			}
			else
			{
				base.AnimatorController.speed = 1f;
			}
		}


		/// <summary>
		/// Scales the capsule for crouching.
		/// </summary>
		/// <param name="crouch">If set to <c>true</c> crouch.</param>
		void ScaleCapsuleForCrouching (bool crouch)
		{
			if (IsGrounded && crouch)
			{
				if (base.IsCrouching) return;
				SetCrouchColliderSize();
				base.IsCrouching = true;
				//StartCoroutine (_OnCrouchDown());
			}
			else
			{
				Ray crouchRay = new Ray(RigidController.position + Vector3.up * CapsuleController.radius * CircleHalf, Vector3.up);
				float crouchRayLength = m_OriginalCapsuleHeight - CapsuleController.radius * CircleHalf;
				if (Physics.SphereCast(crouchRay, CapsuleController.radius * CircleHalf, crouchRayLength))
				{
					SetCrouchColliderSize();
					base.IsCrouching = true;
					return;
				}
				SetCrouchColliderSize(true);
				base.IsCrouching = false;
			}
		}


		/// <summary>
		/// Sets the size of the crouch collider.
		/// しゃがんだ時にcolliderの形状を変える
		/// </summary>
		void SetCrouchColliderSize (bool reset = false)
		{
			base.CapsuleController.height = reset ? m_OriginalCapsuleHeight : m_CrouchingHeight;
			base.CapsuleController.center = reset ? m_OriginalCapsuleCenter : m_CrouchingCenter;
			base.CapsuleController.radius = reset ? m_OriginalCapsuleRadius : m_CrouchingRadius;
		}


		/// <summary>
		/// Prevents the standing in low headroom.
		/// </summary>
		void PreventStandingInLowHeadroom()
		{
			if (!base.IsCrouching)
			{
				Ray crouchRay 			= new Ray(RigidController.position + Vector3.up * CapsuleController.radius * CircleHalf, Vector3.up);
				float crouchRayLength 	= m_OriginalCapsuleHeight - CapsuleController.radius * CircleHalf;
				if (Physics.SphereCast(crouchRay, CapsuleController.radius * CircleHalf, crouchRayLength))
				{
					SetCrouchColliderSize();
					base.IsCrouching = true;
				}
			}
		}


		/// <summary>
		/// Ons the crouch down.
		/// </summary>
		/// <returns>The crouch down.</returns>
		IEnumerator _OnCrouchDown ()
		{
			AnimatorStateInfo info;
			while (true) 
			{
				yield return new WaitForSeconds(0.02f);
				info = base.AnimatorController.GetCurrentAnimatorStateInfo (0);
				if (info.normalizedTime <= 0.9f) 
				{
					Debug.Log (info.normalizedTime);
				}
				else break;
			}
			yield return new WaitForSeconds(0.02f);
		}


		/// <summary>
		/// Sets the state of the attack.
		/// </summary>
		public void SetAttackState ()
		{
			base.AnimatorController.SetBool (CharacterModel.OnTargeting, (base.AttackPattern == ActionPattern.NONE));
		}


		/// <summary>
		/// Raises the animator IK event.
		/// Animator override Inverse Kinematic
		/// </summary>
		public override void OnAnimatorIK ()
		{
			if (base.Target != null) 
			{
				//global weight | body | head | eyes | clamp
				base.AnimatorController.SetLookAtWeight (.5f, 0.8f, .5f, .5f, .5f);
				base.AnimatorController.SetLookAtPosition (base.Target.transform.position);
			}
			else
			{
				base.AnimatorController.SetLookAtWeight (0);
			}
		}



		/// <summary>
		/// Sets the fighting.
		/// </summary>
		/// <param name="action">If set to <c>true</c> action.</param>
		public void SetFighting (bool action = false)
		{
			base.AnimatorController.SetBool (PlayerCharacter.OnKick, true);
			AnimatorStateInfo info = base.AnimatorController.GetCurrentAnimatorStateInfo(0);
			StartCoroutine (
				base.OnWait (
					info.length,
					() => {
					},
					() => {
						base.AnimatorController.SetBool (PlayerCharacter.OnKick, false);
					}
				)
			);
		}


		/// <summary>
		/// Shot this instance.
		/// </summary>
		public override void Shot ()
		{
			base.Shot ();
			/*
			base.AnimatorController.SetTrigger (CharacterModel.OnShot);
			AnimatorStateInfo info = AnimatorController.GetCurrentAnimatorStateInfo(0);
			Debug.Log (info);
			*/
		}

	}



}
