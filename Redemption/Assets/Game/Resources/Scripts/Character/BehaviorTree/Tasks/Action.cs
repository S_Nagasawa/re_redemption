﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;


using Product.Systems;
using Product.Character;


namespace Product.Character.BehaviourTree.Tasks
{

    /// <summary>
    /// Action.
    /// 条件や挙動を管理するタスク Characterにつき複数のAction
    /// </summary>
    public class Action : BehaviourTreeBase
    {
        /// <summary>
        /// Gets a value indicating whether this instance is done.
        /// タスクが終了しているか
        /// </summary>
        /// <value><c>true</c> if this instance is done; otherwise, <c>false</c>.</value>
        public bool IsDone {
            get;
            private set;
        }

        /// <summary>
        /// Gets the transform.
        /// タスクを実行するために向かうべきGameObjectのデータ
        /// </summary>
        /// <value>The transform.</value>
        public Transform transform {
            get;
            private set;
        }

        public override void Initialize ()
        {
            this.IsDone = false;
        }

        public void AddTransform (Transform transform)
        {
            this.transform = transform;
        }

    }



}



