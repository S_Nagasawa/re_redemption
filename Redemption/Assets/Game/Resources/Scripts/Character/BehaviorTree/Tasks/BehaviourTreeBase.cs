﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;


using Product.Systems;
using Product.Character;


namespace Product.Character.BehaviourTree.Tasks
{

    /// <summary>
    /// I behaviour tree.
    /// </summary>
    public interface IBehaviourTree 
    {
        void Initialize ();
    }


    public abstract class BehaviourTreeBase : IBehaviourTree
    {

        public virtual void Initialize () {}

        protected virtual IEnumerator DoInitialize ()
        {
            yield break;
        }
    }


}



