﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;


using Product.Systems;
using Product.Character;
using Product.Character.BehaviourTree.Tasks;

namespace Product.Character.BehaviourTree
{

    /// <summary>
    /// Behaviour manager.
    /// </summary>
    public class BehaviourManager
    {
        public Conditional conditional {
            get;
            private set;
        }
        public List<Product.Character.BehaviourTree.Tasks.Action> actionList {
            get;
            private set;
        }

        public Transform transformCache {
            get;
            private set;
        }

        private bool Disposed = false;

        /// <summary>
        /// Initialize the specified transform, conditional and actionlist.
        /// </summary>
        /// <param name="transform">Transform.</param>
        /// <param name="conditional">Conditional.</param>
        /// <param name="actionlist">Actionlist.</param>
        public void Initialize (Transform transform, Conditional conditional, List<Product.Character.BehaviourTree.Tasks.Action> actionlist)
        {
            this.transformCache = transform;
            this.conditional    = conditional;
            this.actionList     = actionlist;
            AllInitialize ();
        }

        /// <summary>
        /// Alls the initialize.
        /// Taskの初期化
        /// </summary>
        public void AllInitialize ()
        {
            this.conditional.Initialize ();
            this.actionList.ForEach ((Action act) => 
                {
                    act.Initialize();
                }
            );
        }


        /// <summary>
        /// Adds the actions.
        /// </summary>
        /// <param name="transform">Transform.</param>
        public void AddActions (Transform transform)
        {
            this.actionList.ForEach ((Action act) => 
                {
                    act.AddTransform(transform);
                }
            );
        }


        /// <summary>
        /// Dos the dispose.
        /// Killされた事によるTaskの削除
        /// </summary>
        public void DoDispose ()
        {
            if (!Disposed) 
            {
                Disposed = true;
                this.conditional = null;
                this.actionList.Clear ();
            }
        }

    }




}



