﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;


using Product.Systems;
using Product.Character.BehaviourTree;

namespace Product.Character
{
	/// <summary>
	/// Follow character.
	/// </summary>
	[RequireComponent(typeof(WeaponController))]
	public class FollowCharacter : CharacterModel
	{
		public static List<FollowCharacter> FollowCharacterList = new List<FollowCharacter>();

		private Vector3 m_CapsuleCenter;
		private bool 	m_Sneaking;
		private bool 	m_Actioning;
		private float 	m_OrigGroundCheckDistance;
		private float   m_TurnAmount;
		private float   m_ForwardAmount;
		private float 	m_MeshHeight;

		[Tooltip("AIの行動パターン")] public AI_STATE AIState;
		[SerializeField] private float m_RunningSpeed = 1f;
		[SerializeField] private float m_PatrolSpeed  = 0.4f;

		/// <summary>
		/// Parameter Cached
		/// </summary>
		float _RunningSpeed;
		/// <summary>
		/// The nav mesh radius.
		/// </summary>
		float _NavMeshRadius;


		#region Waiting Params
		[SerializeField] private float m_MinWaitTime = 1f;
		[SerializeField] private float m_MaxWaitTime = 10f;
		[SerializeField] private float m_WaitPercent = 40f;
		#endregion

		#region WayPoint AI
		[SerializeField] private WayPointSystem wayPointSystem;
		#endregion

		protected override void OnEnable ()
		{
			FollowCharacterList.Add(this);
		}

		protected override void OnDisable ()
		{
			FollowCharacterList.Remove(this);
		}


		/// <summary>
		/// Awake this instance.
        /// Parameter修正
		/// </summary>
		protected override void Awake ()
		{
			base.Awake ();
			_CreateStatus ();
			_RunningSpeed  = m_RunningSpeed;
			_NavMeshRadius = base.NavMeshController.radius;
		}


		/// <summary>
		/// Create the status.
		/// @TODO 決め打ち
		/// </summary>
		void _CreateStatus ()
		{
			if (base.Status == null)
				base.Status = new CharacterStatus ();
			base.Status.Name    = this.name.ToString ();
			base.Status.Hp      = 2000;
			base.Status.MaxHp   = base.Status.Hp;
			base.Status.Attack  = 500;
			base.Status.Defence = 500;
			base.Status.Weight  = 60f;
		}
			

		/// <summary>
		/// Start this instance.
		/// </summary>
		protected override void Start () 
		{
			base.Start ();
			m_MeshHeight 	= base.CapsuleController.height;
			m_CapsuleCenter = base.CapsuleController.center;
			m_OrigGroundCheckDistance = base.GroundCheckDistance;

			StartCoroutine (_OnStart ());

			base.LifeCycleStream
				.Where(_ => base.Status != null)
				.Subscribe (_ => {
					if (base.Status.IsDie()) 
					{
						base.Kill();
					}
					else
					{
						_HandleStateAI ();
					}
				}).AddTo (this);
		}


		/// <summary>
		/// Ons the start.
		/// </summary>
		/// <returns>The start.</returns>
		private IEnumerator _OnStart ()
		{
			IEnumerator initialize = this.Initialize ();
			while (initialize.MoveNext ()) 
			{
				yield return null;
			}
			yield break;
		}


		/// <summary>
		/// Initialize this instance.
		/// </summary>
		protected override IEnumerator Initialize ()
		{
			base.NavMeshController.updateRotation = false;
			base.NavMeshController.updatePosition = true;

			foreach (CharacterModel list in CharacterModel.CharacterModelList) 
			{
				if (list == this) 
				{
					CharacterModel.CharacterModelList.Remove (list);
				}
			}

			wayPointSystem = new WayPointSystem (base.TransformCache, base.NavMeshController.stoppingDistance, 0);
            wayPointSystem.Initialize ();

			if (wayPointSystem.CanUseSystem()) 
            {
                AIState = AI_STATE.NONE;
            }
            else
            {
                AIState     = AI_STATE.PATROL;
                base.Target = wayPointSystem.CurrentTransform;
            }
            base.CreateAwareness ();
            yield break;
		}


		/// <summary>
		/// Handles the state AI
		/// </summary>
		private void _HandleStateAI ()
		{
			
			AIState = base.GetAIState ((WayPointSystem)wayPointSystem /*_HasShooting ()*/);
			Color _color = Color.white;

			switch (AIState) 
			{
				case AI_STATE.PATROL:
				_color = Color.blue;
				UpdateWayPoint ();
				break;
				case AI_STATE.SHOOTING:
				_color = Color.red;
				UpdateShooting ();
				break;
				case AI_STATE.NONE:
				_color = Color.green;
				UpdateReset ();
				break;
				case AI_STATE.COVER:
				_color = Color.magenta;
				UpdateCovering ();
				break;
			}

			{
				base.NavMeshController.speed  = _GetSpeedControll ();
				base.NavMeshController.radius = (AIState == AI_STATE.COVER || AIState == AI_STATE.SHOOTING) ? _NavMeshRadius/2f : _NavMeshRadius;
				base.WeaponCtrl.SetReadyTarget (AIState == AI_STATE.COVER || AIState == AI_STATE.SHOOTING);
				DebugLines (_color);
			}
		}


		/// <summary>
		/// Has the shooting.
		/// 自分以外の誰かが戦っているか
		/// </summary>
		/// <returns><c>true</c>, if shooting was hased, <c>false</c> otherwise.</returns>
		private bool _HasShooting ()
		{
			return CharacterModelList
				.Where ((CharacterModel arg) => arg != this && arg is FollowCharacter)
				.Any ((CharacterModel arg) => ((FollowCharacter)arg).AIState == AI_STATE.SHOOTING);
		}


		/// <summary>
		/// Gets the speed controll.
		/// </summary>
		/// <returns>The speed controll.</returns>
		private float _GetSpeedControll ()
		{
			if (AIState == AI_STATE.NONE) 
			{
				m_RunningSpeed = _RunningSpeed;
			}

			if (AIState == AI_STATE.PATROL) 
			{
				m_RunningSpeed = wayPointSystem.GetDistanceAutoSpeed ();
			}

			if (AIState == AI_STATE.SHOOTING)
			{
				if (base.TargetRange == TARGET_RANGE.SHORT) 
				{
					m_RunningSpeed = 0.001f;
				}
				else
				{
					m_RunningSpeed = _RunningSpeed / 2f;
				}
			}

			if (AIState == AI_STATE.COVER)
			{
				if (base.TargetRange == TARGET_RANGE.SHORT) 
				{
					m_RunningSpeed = 0.001f;
				}
				else
				{
					m_RunningSpeed = _RunningSpeed;					
				}
			}

			return m_RunningSpeed;
		}


		/// <summary>
		/// Move the specified move, crouch and jump.
		/// </summary>
		/// <param name="move">Move.</param>
		/// <param name="crouch">If set to <c>true</c> crouch.</param>
		/// <param name="jump">If set to <c>true</c> jump.</param>
		public override void Move (Vector3 move, bool crouch = false, bool jump = false)
		{
			if (move.magnitude > 1f) 
				move.Normalize();

			move = base.TransformCache.InverseTransformDirection(move);
			base.CheckGroundStatus();

			move = Vector3.ProjectOnPlane(move, base.GroundNormal);
			m_TurnAmount 	= Mathf.Atan2(move.x, move.z);
			m_ForwardAmount = move.z;
			base.ApplyExtraTurnRotation(m_ForwardAmount, m_TurnAmount);
			if (IsGrounded)
			{
				base.HandleGroundedMovement(crouch, jump);
			}
			UpdateAnimator(move);
			base.UpdateState (move);
			base.UpdateStateAction (move);
		}


		/// <summary>
		/// Updates the animator.
		/// </summary>
		/// <param name="move">Move.</param>
		protected override void UpdateAnimator(Vector3? move = null)
		{
			Vector3 position = move ?? Vector3.zero;
			{
				base.AnimatorController.SetFloat(CharacterModel.OnForward, m_ForwardAmount, 0.1f, Time.deltaTime);
				base.AnimatorController.SetFloat(CharacterModel.OnTurn, m_TurnAmount, 0.1f, Time.deltaTime);
				base.AnimatorController.SetBool (CharacterModel.OnGround, base.IsGrounded);

				if ((AIState == AI_STATE.SHOOTING || AIState == AI_STATE.COVER)) 
				{
					base.AnimatorController.SetBool (CharacterModel.OnTargeting, (base.TargetRange == TARGET_RANGE.SHORT));
				}
				else
				{
					base.AnimatorController.SetBool (CharacterModel.OnTargeting, false);
				}
			}
			float runCycle 	= Mathf.Repeat(base.AnimatorController.GetCurrentAnimatorStateInfo(0).normalizedTime + RunCycleLegOffset, 1);
			float jumpLeg 	= (runCycle < base.CircleHalf ? 1 : -1) * m_ForwardAmount;
			if (base.IsGrounded)
			{
				base.AnimatorController.SetFloat(CharacterModel.OnJumpLeg, jumpLeg);
			}
			if (IsGrounded && position.magnitude > 0)
			{
				base.AnimatorController.speed = base.AnimSpeedMultiplier;
			}
			else
			{
				base.AnimatorController.speed = 1;
			}
		}


		#region WayPointAI Method
		/// <summary>
		/// WayPoint地点に到着したか
		/// </summary>
		bool IsArrivalAt = false;

		/// <summary>
		/// Updates the way point.
		/// </summary>
		/// <param name="move">Move.</param>
		protected override void UpdateWayPoint (Vector3? move = null)
		{
			//エージェントの位置および現在の経路での目標地点の間の距離（読み取り専用）> 目標地点のどれぐらい手前で停止するかの距離
			if (base.NavMeshController.remainingDistance > base.NavMeshController.stoppingDistance)
			{
			}
			else
			{
				wayPointSystem.NextIndex ();				
			}
			Move (base.NavMeshController.desiredVelocity, false, false);
			StartAgent ();
		}


		/// <summary>
		/// Arrivals at way point.
		/// </summary>
		protected override void ArrivalAtWayPoint ()
		{
            StartCoroutine (_OnHandleWait ());
			IsArrivalAt = false;
			wayPointSystem.NextIndex ();
			ResumeAgent();
		}
		#endregion

		System.Action patrolBefore;
		System.Action patrolAfter;
		/// <summary>
		/// Ons the handle wait.
		/// </summary>
		/// <returns>The handle wait.</returns>
		private IEnumerator _OnHandleWait ()
		{
			StopAgent();
			float _waitTime   = (Random.Range (0, 100) > m_WaitPercent) ? m_MaxWaitTime : m_MinWaitTime;
			IEnumerator _wait = base.OnWait (_waitTime, null, null);

			while (_wait.MoveNext ()) 
			{
				yield return null;
			}
			yield return null;
		}


		/// <summary>
		/// Updates the shooting.
		/// </summary>
		/// <param name="move">Move.</param>
		protected override void UpdateShooting (Vector3? move = null) 
		{
			//WayPointがない場合は一旦NavMeshを再開させる
			if (wayPointSystem.CanUseSystem ()) 
			{
				ResumeAgent ();
			}
			Shot ();
			Move (base.NavMeshController.desiredVelocity, false, false);
			StartAgent ();
		}


		/// <summary>
		/// Updates the reset.
		/// </summary>
		/// <param name="move">Move.</param>
		protected override void UpdateReset (Vector3? move = null)
		{
			base.Target     = null;
			base.WeaponCtrl.ResetTarget ();
			Move (Vector3.zero, false, false);
			StopAgent (true);
		}


		/// <summary>
		/// Updates the covering.
		/// カバーリング
		/// </summary>
		/// <param name="move">Move.</param>
		protected override void UpdateCovering (Vector3? move = null) 
		{
			if (base.TargetRange == TARGET_RANGE.SHORT) 
			{
				Shot ();
			}
			Move (base.NavMeshController.desiredVelocity, false, false);
			StartAgent ();
		}


		/// <summary>
		/// Raises the animator IK event.
		/// Animator override Inverse Kinematic
		/// </summary>
		public override void OnAnimatorIK ()
		{
			//base.OnAnimatorIK ();
			if ((AIState == AI_STATE.SHOOTING || AIState == AI_STATE.COVER) && base.Target != null) 
			{
				base.AnimatorController.SetLookAtWeight (.5f, .8f, .5f, .5f, .5f);
				base.AnimatorController.SetLookAtPosition (base.Target.transform.position);
			}
			else
			{
				base.AnimatorController.SetLookAtWeight (0);
			}
		}


		#region Agent
		/// <summary>
		/// Stops the agent.
		/// </summary>
		/// <param name="resetTarget">If set to <c>true</c> reset target.</param>
		public void StopAgent (bool resetTarget = false)
		{
			if (resetTarget) 
			{
				base.NavMeshController.Stop (true);							
			}
			else
			{
				base.NavMeshController.Stop ();				
			}
		}

        public void ResumeAgent ()
        {
            base.NavMeshController.Resume ();
        }

		/// <summary>
		/// Start the agent.
		/// Targetを管理
		/// </summary>
		public void StartAgent ()
		{
			switch (AIState) 
			{
				//Patrol
				case AI_STATE.PATROL:
				{
					base.WeaponCtrl.ResetTarget ();
					base.Target = wayPointSystem.CurrentTransform;
				}
				break;

				//Shooting
				case AI_STATE.SHOOTING:
				{
					base.Target = base.AwarenessController.Target;
					base.WeaponCtrl.Target = base.Target;
				}
				break;

				//Cover
				case AI_STATE.COVER:
				{
					base.Target = GetCoveringTarget();
					base.WeaponCtrl.Target = base.Target;
				}
				break;
			}
			if (base.Target != null) 
			{
				base.NavMeshController.SetDestination(base.Target.position);
			}
		}
		#endregion


		/// <summary>
		/// Debugs the lines.
		/// </summary>
		/// <param name="color">Color.</param>
		void DebugLines (Color color) 
		{
			if (base.Target == null)
				return;

			#if UNITY_EDITOR
			Debug.DrawLine (base.TransformCache.position, base.Target.position, color);
			#endif
		}


		/// <summary>
		/// Gets the covering target.
		/// 同士討ちはさせないようにする
		/// </summary>
		/// <returns>The covering target.</returns>
		private Transform GetCoveringTarget ()
		{
			Transform tr = null;

			//自分たちと同クラスでのTarget
            FollowCharacter.FollowCharacterList.ForEach ((FollowCharacter follow) => {
                //FollowCharacter以外のtarget
                if (follow.Target != null && follow != this)
                {
                    tr = follow.Target;
                    Debug.Log (follow.Target);
                }
            });
			return tr;
		}



	}


}