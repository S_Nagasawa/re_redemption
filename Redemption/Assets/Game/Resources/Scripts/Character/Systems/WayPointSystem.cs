﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Way point system.
/// </summary>
namespace Product.Systems
{
    [System.Serializable]
    public class WayPointData
    {
        public Transform transform {
            get;
            private set;
        }
        public bool Arrived {
            get;
            set;
        }
    }


	[System.Serializable]
	public class WayPointSystem
	{
		private static float MIN_SPEED = 0.4f;
		private static float MAX_SPEED = 1.2f;

		/// <summary>
		/// GetTagName
		/// </summary>
		private const string NAME = "WayPointSystem";

		/// <summary>
		/// WayPoints transform Array
		/// </summary>
		public List<Transform> WayPointTransform;

		/// <summary>
		/// 対象のTransform
		/// </summary>
		/// <value>The current transform.</value>
		public Transform CurrentTransform
		{
			get {
				Transform tr = null;
				if (!WayPointTransform.IsNullOrEmpty<Transform> ()) 
				{
					tr = WayPointTransform [Index];
				}
				return tr;
			}
		}

		/// <summary>
		/// AI Transform
		/// </summary>
		/// <value>The move transform.</value>
		public Transform MoveTransform {
			get;
			private set;
		}

		/// <summary>
		/// ArrayのN番
		/// </summary>
		public int Index {
            get {
                return index;
            }
            set {
                index = value;
            }
		}
        [SerializeField] private int index;

		/// <summary>
		/// AB地点の距離
		/// </summary>
		/// <value>The distance.</value>
		public float Distance {
			get;
			private set;
		}


        /// <summary>
        /// Initializes a new instance of the <see cref="Product.Systems.WayPointSystem"/> class.
        /// </summary>
        /// <param name="moveTransform">Move transform.</param>
        /// <param name="distance">Distance.</param>
        /// <param name="index">Index.</param>
		public WayPointSystem (Transform moveTransform, float distance, int index)
		{
			this.MoveTransform = moveTransform;
            this.Distance      = distance;
			this.Index         = index;
		}


		/// <summary>
		/// Nexts the index.
		/// </summary>
		public void NextIndex ()
		{
            if ((WayPointTransform.Count - 1) == Index) 
			{
                ResetIndex ();
			}
			else 
			{
				Index++;
			}
		}


		/// <summary>
		/// Resets the index.
		/// </summary>
		public void ResetIndex ()
		{
			Index = 0;
		}


        /// <summary>
        /// Initialize this instance.
        /// </summary>
		public void Initialize ()
		{
			ResetIndex ();
			GameObject[] gos = GameObject.FindGameObjectsWithTag (WayPointSystem.NAME);
			int index        = Random.Range (0, gos.Length - 1);
			//最初に並び替え
			if (gos.Length > 0 && gos != null) 
			{
				Transform[] waypoint = gos[index].GetComponentsInChildrenWithoutSelf<Transform>();
				WayPointTransform    = waypoint.ToList ();

				WayPointTransform.Sort (delegate(Transform a, Transform b) 
					{
						return Vector3.Distance (MoveTransform.position, a.position)
							.CompareTo((Vector3.Distance(MoveTransform.position, b.position)));
					}
				);

				/*
				* Array 
				System.Array.Sort (waypoint, (Transform a, Transform b) 
					=> Vector3.Distance (MoveTransform.position, a.position)
					.CompareTo ((Vector3.Distance (MoveTransform.position, b.position))
				));
				*/

				/*
				* Linq
				WayPointTransform = waypoint.OrderBy(e => Vector3.Distance(e.transform.position, MoveTransform.position)).ToList();
				*/
			}
		}


		/// <summary>
		/// Determines whether this instance is way points empty.
		/// Listがempty且つCurrentTransformがnullか
		/// </summary>
		/// <returns><c>true</c> if this instance is way points empty; otherwise, <c>false</c>.</returns>
		public bool CanUseSystem ()
		{
			return WayPointTransform.IsNullOrEmpty<Transform> () && CurrentTransform == null;
		}

		/// <summary>
		/// Determines whether this instance has next way point.
		/// </summary>
		/// <returns><c>true</c> if this instance has next way point; otherwise, <c>false</c>.</returns>
		public bool HasNextWayPoint ()
		{
			return (CurrentTransform.position - MoveTransform.position).magnitude < Distance;
		}

		/// <summary>
		/// Gets the distance auto speed.
		/// </summary>
		/// <returns>The distance auto speed.</returns>
		public float GetDistanceAutoSpeed ()
		{
			float dist = (CurrentTransform.position - MoveTransform.position).magnitude * 0.05f;

			if (dist > WayPointSystem.MAX_SPEED) 
			{
				dist = MAX_SPEED;
			}

			return dist > WayPointSystem.MIN_SPEED ? dist : WayPointSystem.MIN_SPEED;
		}

	}
		


}

