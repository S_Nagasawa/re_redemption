﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using System.Linq;
using UniRx;
using UniRx.Triggers;


using Product.Character;

namespace Product.Systems
{
	/// <summary>
	/// Player controller.
	/// key操作メイン　Playerの各Classをcallする
	/// </summary>
	public class PlayerController : MonoBehaviour
	{
		const string JUMP       = "Jump";
		const string VERTICAL   = "Vertical";
		const string HORIZONTAL = "Horizontal";

		private PlayerCharacter m_Player;
		private Vector3 		m_CamForward;
		private Vector3 		m_Move;

		private bool m_Jump       = false;
		private bool m_Crounch    = false;


		Transform        _playerTransform;
		ActionPattern    _actionPattern;
		WeaponController _weaponCtrl;
		[SerializeField] private Transform 	m_Cam;
		[SerializeField] private Transform 	m_Pivot;


		#region IK Stuff
		public float point 				= 30f;
		public float screenTouchSpeed 	= 20f;
		private Vector3 m_TouchPoint 	= Vector3.zero;
		private Vector3 m_MouseOrigin;
		[SerializeField] private float m_AnglePerPixelX = 0f;
		[SerializeField] private float m_AnglePerPixelY = 0f;
		#endregion


		[SerializeField] float _pivotY;


		/// <summary>
		/// Start this instance.
		/// </summary>
		private void Start()
		{
			if (Camera.main != null) 
			{
				m_Cam = Camera.main.transform;
			}
			else 
			{
				Debug.LogWarning("Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
			}

			{
				m_Pivot          = m_Cam.parent.transform;
				m_Player         = GetComponent<PlayerCharacter>() as PlayerCharacter;
				_playerTransform = m_Player.TransformCache;
				_weaponCtrl      = m_Player.WeaponCtrl;
				_pivotY          = m_Pivot.position.y;
			}

			//GetAction State
			this.UpdateAsObservable ()
				.Where (_ => _DontDestroyPlayer ())
				.Subscribe (_ => { _actionPattern = m_Player.AttackPattern; })
				.AddTo (this);

			//Jump
			this.UpdateAsObservable ()
				.Where (_ => _DontDestroyPlayer ())
				.Subscribe (_ => { m_Jump = CrossPlatformInputManager.GetButtonDown (JUMP); })
				.AddTo (this);

			//Crouch しゃがむ
			this.UpdateAsObservable ()
				.Where (_ => _DontDestroyPlayer ())
				.Subscribe (_ => { m_Crounch = Input.GetKey (KeyCode.C); })
				.AddTo (this);

			//Move
			this.UpdateAsObservable ()
				.Where (_ => _DontDestroyPlayer () && m_Cam != null)
				.Select (_ => 
					m_Move = (CrossPlatformInputManager.GetAxis (VERTICAL) * m_CamForward) + 
						(CrossPlatformInputManager.GetAxis (HORIZONTAL) * m_Cam.right)
				)
				.Select (_ => 
					m_CamForward = Vector3.Scale (m_Cam.forward, new Vector3 (1, 0, 1)).normalized
				)
				.Subscribe (_ => { m_Player.Move (m_Move, m_Crounch, m_Jump); })
				.AddTo (this);

			//ShotIdle
			this.UpdateAsObservable ()
				.Where(_ => Input.GetKey (KeyCode.Tab))
				.Subscribe (_ => { _OnAttackState(); })
				.AddTo (this);

			//Shot
			this.UpdateAsObservable ()
				.Where (_ => _actionPattern != ActionPattern.NONE && Input.GetKey (KeyCode.Z))
				.Subscribe (_ => { _OnShotAction(); })
				.AddTo (this);

			//Aim move
			this.UpdateAsObservable ()
				.Where (_ => _actionPattern != ActionPattern.NONE)
				.Subscribe (_ => { _HandRotateDamping(); })
				.AddTo (this);

		}


		/*
		private void FixedUpdate()
		{
			#if !MOBILE_INPUT
			// walk speed multiplier
			if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
			#endif
		}
		*/


		/// <summary>
		/// Dont destroy player.
		/// </summary>
		/// <returns><c>true</c>, if destroy player was donted, <c>false</c> otherwise.</returns>
		private bool _DontDestroyPlayer ()
		{
			return (m_Player != null && !m_Player.Status.IsDie ());
		}


		/// <summary>
		/// Hands the rotate damping.
		/// </summary>
		private void _HandRotateDamping ()
		{
			if(Input.GetMouseButtonDown(0))
			{
				m_MouseOrigin = Input.mousePosition;
			}
			// 右ボタンが押されてたら現在地と前の位置の差分をとって動いた距離に加算しておく
			m_TouchPoint += Input.mousePosition - m_MouseOrigin;
			// 画面の高さだけマウス動かしたときに何度回転させるか
			m_AnglePerPixelX = 180.0f / Screen.width;
			m_AnglePerPixelY = 60.0f / Screen.height;
			// 移動距離と上で求めた値をかけて角度を求める
			var angleX = Mathf.Repeat(m_AnglePerPixelX * m_TouchPoint.x, 360.0f);        // X方向
			var angleY = Mathf.Clamp(m_AnglePerPixelY * -m_TouchPoint.y, -10.0f, 60.0f); // Y方向
			//Quaternion _rotation = Quaternion.Euler(angleY, angleX, 0);
			//m_Player.BodyRotation = new Vector3 (angleX, angleY, 0f);
		}


		/// <summary>
		/// Ons the shot action.
		/// </summary>
		private void _OnShotAction ()
		{
			m_Player.Shot();
		}
			

		/// <summary>
		/// Ons the state of the attack.
		/// </summary>
		private void _OnAttackState ()
		{
			m_Player.SetAttackState ();
		}
	}



}
