﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;

using Product.Character;

namespace Product.Systems
{
	/// <summary>
	/// Awareness.
	/// キャラクターの視野
	/// 自身もGame視野内かの確認
	/// LayerMaskはCharacterModelで定義
	/// Skin Mesh RendererがないGameObjectだと呼ばれない
	/// </summary>
	public class Awareness : MonoBehaviour 
	{
		private static readonly string MAIN_CAMERA_TAG_NAME = "MainCamera";

		public CharacterModel Model  { get; set; }
		public NavMeshAgent   Agent  { get; set; }
		public Transform      Target { get; private set; }

		/// <summary>
		/// キャラクターの視野
		/// </summary>
		/// <value>The remaining distance.</value>
		public float Distance { get; set; }

		[Tooltip("Characterが本来見えるはずの視界値")] public float FieldOfViewAngle   = 120f;
		[Tooltip("Characterが攻撃開始の視界値")]      public float FieldOfAttackAngle = 150f;

		[SerializeField] private bool m_CharacterInSight;
		[SerializeField] private bool m_TargetCaptures;


		private bool _isRendered = false;//カメラに表示されているか
		public bool IsRendered
		{
			get { 
				return _isRendered;
			}
		}


		/// <summary>
		/// The height of the m max sight.
		/// </summary>
		[SerializeField][Tooltip("キャラクターが見上げた際の限界高さ")] 
		float m_MaxSightHeight = 6f;

		void Start () 
		{
			//Targetがいるか
			this.UpdateAsObservable ()
				.Where (_ => Target == null)
				.Subscribe (_ => {
					Target = GetTarget ();
				}).AddTo (this);

			//
			this.UpdateAsObservable ()
				.Where (_ => Target != null)
				.Subscribe (_ => {
					m_CharacterInSight = _IsCharacterSighting();
					m_TargetCaptures   = _IsCapturingTarget();
				}).AddTo (this);
		}


		/// <summary>
		/// Gets the target.
		/// 都度targetを取得しに行く
		/// @todo : 奇襲された場合の実装は未実装
		/// </summary>
		private Transform GetTarget()
		{
			Transform tr = null;
			foreach (CharacterModel chara in CharacterModel.CharacterModelList) 
			{
				//playerとの向きと距離
				//1 一定の距離内か 2視野内120度内にいるか
				if(
					(Model.TransformCache.position - chara.TransformCache.position).sqrMagnitude < FieldOfAttackAngle && 
					Vector3.Angle(chara.TransformCache.position - Model.TransformCache.position, transform.forward) <= FieldOfViewAngle)
				{
					tr = chara.TransformCache;
				}
			}
			Debug.Log (tr);
			return tr;
		}


		/// <summary>
		/// Update this instance.
		/// UpdateAsObservableが効かない
		/// </summary>
		private void Update () 
		{
			if (_isRendered) 
			{
				WhenInSight ();
			}
			_isRendered = false;
		}


		/// <summary>
		/// Whens the in sight.
		/// </summary>
		public void WhenInSight ()
		{
			//do something
		}


		/// <summary>
		/// Raises the will render object event.
		/// </summary>
		private void OnWillRenderObject ()
		{
			//メインカメラに映った時だけ_isRenderedを有効に
			if (Camera.current.tag == Awareness.MAIN_CAMERA_TAG_NAME) 
			{
				_isRendered = true;
			}
		}


		/// <summary>
		/// Determines whether this instance is character sighting.
		/// </summary>
		/// <returns><c>true</c> if this instance is character sighting; otherwise, <c>false</c>.</returns>
		private bool _IsCharacterSighting ()
		{
			bool _sighting = false;

			//playerとの向きと距離
			//1 一定の距離内か 2視野内120度内にいるか
			if(
				(Model.TransformCache.position - Target.position).sqrMagnitude < FieldOfAttackAngle && 
				Vector3.Angle(Target.position - Model.TransformCache.position, transform.forward) <= FieldOfViewAngle)
			{
				_sighting = true;
			}

			return _sighting;
		}


		/// <summary>
		/// 視界がクリアかどうか
		/// </summary>
		/// <returns><c>true</c> if this instance is capturing target; otherwise, <c>false</c>.</returns>
		private bool _IsCapturingTarget ()
		{
			bool _ishit = false;

			NavMeshHit hit;
			if (!Agent.Raycast (Target.position, out hit)) 
			{
				//playerとの距離が規定値内であるか
				_ishit = (Model.TransformCache.position - Target.position).magnitude < Distance;				
			}

			return _ishit;
		}


		/// <summary>
		/// 壁判定
		/// </summary>
		/// <returns><c>true</c> if this instance is target catches; otherwise, <c>false</c>.</returns>
		public bool IsTargetCatches ()
		{
			return m_TargetCaptures;
		}


		/// <summary>
		/// 視野判定
		/// </summary>
		/// <returns><c>true</c> if this instance is character sighting; otherwise, <c>false</c>.</returns>
		public bool IsCharacterSighting ()
		{
			return m_CharacterInSight;
		}


		/// <summary>
		/// 壁、視野判定
		/// </summary>
		/// <returns><c>true</c> if this instance is both capture; otherwise, <c>false</c>.</returns>
		public bool IsBothCapture ()
		{
			return m_TargetCaptures && m_CharacterInSight;
		}
	}

}

