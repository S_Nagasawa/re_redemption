﻿using UnityEngine;
using System.Collections;

/// <summary>
/// I character status.
/// </summary>
namespace Product.Systems
{

	/// <summary>
	/// I character status.
	/// </summary>
	public interface ICharacterStatus
	{
		bool IsDie();
		bool IsHpHalf ();
		bool IsHpQuart ();
	}

	/// <summary>
	/// Character status.
	/// </summary>
	[System.Serializable]
	public class CharacterStatus : ICharacterStatus
	{
		public string Name;
		[SerializeField] private int m_Hp;
		public int Hp {
			get { 
				return m_Hp;
			}
			set { 
				//m_Hp = Mathf.Clamp (value, m_MinHp, MaxHp);
				m_Hp = value;
			}
		}
		private int   m_MinHp = 0;
		public  int   MaxHp;   //最大HP
		public  int   Attack;  //攻撃力
		public  int   Defence; //守備
		public  float Weight;  //重さ
		/// <summary>
		/// Determines whether this instance is die.
		/// </summary>
		/// <returns><c>true</c> if this instance is die; otherwise, <c>false</c>.</returns>
		public bool IsDie ()
		{
			return (Hp <= 0);
		}
		/// <summary>
		/// Determines whether this instance is hp half.
        /// HPが1/2か
        /// </summary>
		/// <returns><c>true</c> if this instance is hp half; otherwise, <c>false</c>.</returns>
		public bool IsHpHalf ()
		{
			return (Hp <= (float)Hp / 2);
		}
		/// <summary>
		/// Determines whether this instance is hp quart.
        /// HPが1/4か
		/// </summary>
		/// <returns><c>true</c> if this instance is hp quart; otherwise, <c>false</c>.</returns>
		public bool IsHpQuart ()
		{
			return (Hp <= (float)Hp / 4);
		}
        /// <summary>
        /// Determines whether this instance is hp full.
        /// HPがMaxか
        /// </summary>
        /// <returns><c>true</c> if this instance is hp full; otherwise, <c>false</c>.</returns>
        public bool IsHpFull ()
        {
            return Hp == MaxHp;
        }
		/// <summary>
		/// Attacks the H.
		/// HP攻撃
		/// </summary>
		/// <param name="attack">Attack.</param>
		public void AttackHP (int attack)
		{
			Hp = (Hp - attack);
		}
		/// <summary>
		/// Recovers the H.
		/// HP回復
		/// </summary>
		/// <param name="recover">Recover.</param>
		public void RecoverHP (int recover)
		{
			Hp = (Hp + recover);
		}



	}




}


