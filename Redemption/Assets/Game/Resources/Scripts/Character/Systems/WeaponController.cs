﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;

using Product.Weapons.Weapon;
using Product.Weapons.Tools;

namespace Product.Systems
{
	/// <summary>
	/// Weapon controller.
	/// Bullet_Root
	/// List<WeaponBase>
	/// を管理
	/// </summary>
	public class WeaponController : MonoBehaviour
	{
		#region Target
		/// <summary>
		/// Gets or sets the target.
		/// </summary>
		/// <value>The target.</value>
		public Transform Target {
			get { 
				return m_Target;
			}
			set { 
				m_Target = value;
			}
		}
		private Transform m_Target;
		#endregion


		#region Valiables
		/// <summary>
		/// ArrayのN番
		/// </summary>
		[SerializeField] private int m_Index;
		public int Index {
			get { 
				return m_Index;
			}
			set { 
				m_Index = value;
			}
		}
			
		/// <summary>
		/// Gets or sets the root transform.
		/// </summary>
		/// <value>The root transform.</value>
		public Transform TransformCache {
			get;
			set;
		}

		/// <summary>
		/// The m weapon transform list.
		/// </summary>
		[SerializeField] private List<WeaponBase> WeaponList;

		/// <summary>
		/// BulletRoot Classのprefab
		/// </summary>
		public GameObject m_BulletRootPrefab;

		/// <summary>
		/// Gets or sets the root.
		/// </summary>
		/// <value>The root.</value>
		public BulletRoot Root {
			get;
			set;
		}

		/// <summary>
		/// The state of the m current weapon.
		/// 現在使える武器
		/// </summary>
		public WeaponBase CurrentWeapon {
			get {
				return WeaponList [Index];
			}
		}
		#endregion


		/// <summary>
		/// Awake this instance.
		/// </summary>
		private void Awake ()
		{
			TransformCache = transform;
			WeaponBase[] weaponArray = TransformCache.gameObject.GetComponentsInChildrenWithoutSelf<WeaponBase> ();
			WeaponList = new List<WeaponBase> (weaponArray);
			ResetIndex ();
			foreach (WeaponBase w in weaponArray) 
			{
				#if UNITY_EDITOR
				//Debug.Log (w);
				#endif
			}
		}


		/// <summary>
		/// Determines whether this instance is weapons null empty.
		/// WeaponListが空じゃないかどうか 空の場合false
		/// </summary>
		/// <returns><c>true</c> if this instance is weapons null empty; otherwise, <c>false</c>.</returns>
		private bool IsWeaponsNullEmpty ()
		{
			return (WeaponList.IsNullOrEmpty<WeaponBase>() == false);
		}


		/// <summary>
		/// Nexts the index.
		/// </summary>
		public void NextIndex ()
		{
			if (WeaponList.Count - 1 == m_Index) 
			{
				m_Index = 0;
			}
			else 
			{
				m_Index++;
			}
		}


		/// <summary>
		/// Resets the index.
		/// </summary>
		private void ResetIndex ()
		{
			m_Index = 0;
		}


		/// <summary>
		/// Visible the specified visibility.
		/// </summary>
		/// <param name="visibility">If set to <c>true</c> visibility.</param>
		public void Visible (bool visibility = false)
		{
			CurrentWeapon.gameObject.SetActive (visibility);
		}


		/// <summary>
		/// Creates the bullets.
		/// </summary>
		public void CreateBulletRoot ()
		{
			if (m_BulletRootPrefab != null) 
			{
				var go   = (GameObject)Instantiate (m_BulletRootPrefab, Vector3.zero, Quaternion.identity);
				go.name  = m_BulletRootPrefab.name;
				Root     = go.GetComponent<BulletRoot> () as BulletRoot;
				if (BulletController.Instance != null) 
				{
					go.transform.SetParent (BulletController.Instance.TransformCache);
				}
				_UpdateCurrentWeapons ();
			}
		}


		/// <summary>
		/// Updates the current weapons.
		/// </summary>
		public void UpdateCurrentWeapons ()
		{
			NextIndex ();
			_UpdateCurrentWeapons ();
		}


		/// <summary>
		/// Determines whether this instance is empty target.
		/// </summary>
		/// <returns><c>true</c> if this instance is empty target; otherwise, <c>false</c>.</returns>
		private bool _IsEmptyTarget ()
		{
			return Target == null;
		}


		/// <summary>
		/// Updates the current weapons.
		/// </summary>
		private void _UpdateCurrentWeapons ()
		{
			//発射口をセットする
			CurrentWeapon.Root = Root;
			Root.Muzzle        = CurrentWeapon.BulletPointTransform;
		}


		/// <summary>
		/// Resets the target.
		/// </summary>
		public void ResetTarget ()
		{
			Target = null;
			foreach (WeaponBase w in WeaponList) 
			{
				w.Target = null;
			}
		}


		/// <summary>
		/// Shot this instance.
		/// </summary>
		public void Shot ()
		{
			if (!_IsEmptyTarget ())
				CurrentWeapon.Target = Target;

			if (CurrentWeapon.CanShot ()) 
			{
				CurrentWeapon.Shot ();
				Root.Shot ();
			}
		}


		/// <summary>
		/// Sets the ready target.
		/// </summary>
		public void SetReadyTarget (bool targeting)
		{
			CurrentWeapon.IsTargeting = targeting;
		}

	}


}

