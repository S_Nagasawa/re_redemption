﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;


#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Product.Tools.Model
{
	/// <summary>
	/// Mesh combine creater.
	/// メッシュ結合ツール
	/// </summary>
	public class MeshCombineCreater : MonoBehaviour 
	{

		#if UNITY_EDITOR

		public GameObject GeneratedObject;

		const string EDITOR_ONLY = "EditorOnly";

		[ContextMenu("Export")]
		void Init ()
		{
			Component[] meshFilters = GetComponentsInChildren<MeshFilter>(true);
			Dictionary<Material, List<CombineInstance>> combineMeshInstanceDictionary 
			= new Dictionary<Material, List<CombineInstance>> ();

			foreach (var mesh in meshFilters) 
			{
				Renderer ren = mesh.gameObject.GetComponent<Renderer> ();
				//var mat = mesh.renderer.sharedMaterial ; BAD
				var mat      = ren.sharedMaterial;

				if (mat == null)
					continue;

				if (!combineMeshInstanceDictionary.ContainsKey (mat)) 
				{
					combineMeshInstanceDictionary.Add (mat, new List<CombineInstance> ());
				}
				var instance = combineMeshInstanceDictionary [mat];
				var cmesh = new CombineInstance ();
				cmesh.transform = mesh.transform.localToWorldMatrix;
				cmesh.mesh = ((MeshFilter)mesh).sharedMesh;
				instance.Add (cmesh);
			}


			gameObject.SetActive (false);
			gameObject.tag = EDITOR_ONLY;

			if (GeneratedObject == null)
				GeneratedObject = new GameObject (name);


			foreach (var dic in combineMeshInstanceDictionary) 
			{
				var newObject = new GameObject (dic.Key.name);
				newObject.isStatic = true;

				var meshRenderer = newObject.AddComponent <MeshRenderer>();
				var meshFilter   = newObject.AddComponent<MeshFilter> ();

				meshRenderer.material = dic.Key;

				Mesh mesh = new Mesh ();
				mesh.CombineMeshes(dic.Value.ToArray());
				Unwrapping.GenerateSecondaryUVSet( mesh);
				meshFilter.sharedMesh = mesh;
				newObject.transform.parent = GeneratedObject.transform;

				Debug.Log (Application.loadedLevelName);
				System.IO.Directory.CreateDirectory( "Assets/Game/" + Application.loadedLevelName + "/" + name );
				AssetDatabase.CreateAsset(mesh, "Assets/Game/" + Application.loadedLevelName+ "/" + name + "/" + dic.Key.name + ".asset");
			}
		}


		/// <summary>
		/// Raises the enable event.
		/// </summary>
		void OnEnable ()
		{
			if (GeneratedObject != null) 
			{
				GeneratedObject.SetActive (false);
			}
		}


		/// <summary>
		/// Raises the disable event.
		/// </summary>
		void OnDisable()
		{
			if (GeneratedObject != null) 
			{
				GeneratedObject.SetActive(true);
			}
		}


		#endif
	}

}

