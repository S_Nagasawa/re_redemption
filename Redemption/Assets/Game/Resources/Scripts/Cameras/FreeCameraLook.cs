﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;

namespace Product.Cameras 
{

	public class FreeCameraLook : Product.Cameras.Pivot 
	{
		/// <summary>
		/// TOUCH_STATE
		/// </summary>
		public enum TOUCH_STATE : byte
		{
			UNZOOM = 0,
			ZOOM,
		}

		[SerializeField] private TOUCH_STATE m_State;
		[SerializeField] private float moveSpeed     = 5f;
		[SerializeField] private float turnSpeed     = 1.5f;
		[SerializeField] private float turnsmoothing = 0.4f;
		[SerializeField] private float tiltMax       = 75f;
		[SerializeField] private float tiltMin       = 45f;
		[SerializeField] private bool lockCursor     = false;


		private float lookAngle;
		private float tiltAngle;

		private const float LookDistance = 100f;

		private float smoothX = 0;
		private float smoothY = 0;
		private float smoothXvelocity = 0;
		private float smoothYvelocity = 0;

		float offsetX;
		float offsetY;
		float aimingWeight;
		//public Crosshair activeCrosshair;
		//public float crosshairOffsetWiggle = 0.2f;

		[SerializeField] float m_MinFov    = 50f;
		[SerializeField] float m_MaxFov    = 70f;
		[SerializeField] float m_ZoomDamping = 2f;

		protected override void Awake ()
		{
			base.Awake();

			Screen.lockCursor = lockCursor;
			m_State           = TOUCH_STATE.ZOOM;
			if(lockCursor)
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
		}

		void Start ()
		{
			ChangeCrosshair();

			IObservable<long> lockCursorStream = Observable.EveryUpdate ().Where (_ => lockCursor && Input.GetMouseButtonUp (0));
			IObservable<long> clickStream      = Observable.EveryUpdate ().Where (_ => !base.CanScreenTouches () && Input.GetMouseButtonDown (0));

			//lock cursol stream
			lockCursorStream
				.Where(_ => !base.CanScreenTouches ())
				.Subscribe (_ => {
					Screen.lockCursor = lockCursor;
				}).AddTo (this);

			//touch state stream
			clickStream
				.Subscribe (_ => {
					m_State = (m_State == TOUCH_STATE.ZOOM) ? TOUCH_STATE.UNZOOM : TOUCH_STATE.ZOOM;
					StartCoroutine(_OnHandleZoom());
				}).AddTo (this);
		}


		/// <summary>
		/// Raises the disable event.
		/// </summary>
		void OnDisable()
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible   = true;
		}


		/// <summary>
		/// Changes the crosshair.
		/// </summary>
		public void ChangeCrosshair ()
		{
			//activeCrosshair = GameObject.FindGameObjectWithTag("CrosshairManager").GetComponent<CrosshairManager>().activeCrosshair;
		}


		/// <summary>
		/// Update this instance.
		/// </summary>
		protected override void Update () 
		{
			base.Update();
			_HandleRotationMovement();
		}


		/// <summary>
		/// Ons the handle zoom.
		/// </summary>
		/// <returns>The handle zoom.</returns>
		private IEnumerator _OnHandleZoom ()
		{
			float fromVal = (m_State == TOUCH_STATE.ZOOM) ? m_MinFov : m_MaxFov;	
			float toVal   = (m_State == TOUCH_STATE.ZOOM) ? m_MaxFov : m_MinFov;
			float time    = 0.0f;
			while (time < m_ZoomDamping) 
			{
				time += Time.deltaTime;
				base.MainCam.fieldOfView = Mathf.Lerp (fromVal, toVal, time/m_ZoomDamping);
				yield return null;
			}
			base.MainCam.fieldOfView = toVal;
			yield return null;
		}


		/// <summary>
		/// Follow the specified deltaTime.
		/// </summary>
		/// <param name="deltaTime">Delta time.</param>
		protected override void Follow (float deltaTime)
		{
			transform.position = Vector3.Lerp(transform.position, target.position, deltaTime * moveSpeed);
		}


		/// <summary>
		/// Handles the offsets.
		/// </summary>
		void HandleOffsets()
		{
			if(offsetX != 0)
			{
				offsetX = Mathf.MoveTowards(offsetX,0,Time.deltaTime);
			}

			if(offsetY != 0)
			{
				offsetY = Mathf.MoveTowards(offsetY,0,Time.deltaTime);
			}
		}
			

		/// <summary>
		/// Handles the rotation movement.
		/// </summary>
		private void _HandleRotationMovement()
		{
			if (base.CanScreenTouches ())
				return;

			HandleOffsets();

			float x = Input.GetAxis("Mouse X") + offsetX;
			float y = Input.GetAxis("Mouse Y") + offsetY;

			if(turnsmoothing > 0)
			{
				smoothX = Mathf.SmoothDamp(smoothX, x, ref smoothXvelocity, turnsmoothing);
				smoothY = Mathf.SmoothDamp(smoothY, y, ref smoothYvelocity, turnsmoothing);
			}
			else
			{
				smoothX = x;
				smoothY = y;
			}

			lookAngle += smoothX*turnSpeed;

			transform.rotation = Quaternion.Euler(0f, lookAngle, 0);

			tiltAngle -= smoothY * turnSpeed;
			tiltAngle = Mathf.Clamp(tiltAngle, - tiltMin, tiltMax);

			pivot.localRotation = Quaternion.Euler(tiltAngle,0,0);

			/*
			if(x > crosshairOffsetWiggle || x < -crosshairOffsetWiggle || y > crosshairOffsetWiggle || y <-crosshairOffsetWiggle)
			{
				activeCrosshair.WiggleCrosshair();
			}
			*/
		}

		/*
		public void WiggleCrosshairAndCamera(WeaponControl weapon, bool shoot)
		{
			activeCrosshair.WiggleCrosshair();

			if(shoot)
			{
				offsetY = weapon.Kickback;
			}
		}
		*/
	}


}

