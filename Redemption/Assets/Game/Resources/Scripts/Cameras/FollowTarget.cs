﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Product;
using System.Linq;
using UniRx;
using UniRx.Triggers;

namespace Product.Cameras 
{

	public abstract class FollowTarget : MonoBehaviour
	{

		[SerializeField] public Transform target;
		[SerializeField] private bool autoTargetPlayer = true;


		public Transform Target
		{
			get {
				return this.target;
			}
		}

		protected virtual void Start()
		{
			if(autoTargetPlayer)
			{
				FindTargetPlayer();
			}
			this.FixedUpdateAsObservable ()
				.Where (_ => (autoTargetPlayer && (target == null || !target.gameObject.activeSelf)))
				.Subscribe (_ => {
					FindTargetPlayer();
				}).AddTo (this);

			/*
			//Follow
			FixedUpdateAsObservable ()
				.Where (_ => target != null && 
					(target.GetComponent<Rigidbody>() != null && !target.GetComponent<Rigidbody>().isKinematic)
				)
				.Subscribe (_ => 
					{
						Follow(Time.deltaTime);
					}
				).AddTo (this);
			*/
		}

		void FixedUpdate () 
		{
			/*
			if(autoTargetPlayer && (target == null || !target.gameObject.activeSelf))
			{
				FindTargetPlayer();
			}
			*/

			if(target != null && (target.GetComponent<Rigidbody>() != null && !target.GetComponent<Rigidbody>().isKinematic))
			{
				Follow(Time.deltaTime);
			}
		}

		protected abstract void Follow(float deltaTime);


		public void FindTargetPlayer()
		{
			if(target == null)
			{
				GameObject targetObj = GameObject.FindGameObjectWithTag(Product.Tags.PLAYER);

				if(targetObj)
				{
					SetTarget(targetObj.transform);
				}
			}
		}

		public virtual void SetTarget(Transform newTransform)
		{
			target = newTransform;
		}


	}


}

