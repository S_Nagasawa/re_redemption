﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

namespace Product.Cameras 
{
	/// <summary>
	/// Pivot.
	/// </summary>
	[ExecuteInEditMode]
	public class Pivot : Product.Cameras.FollowTarget 
	{

		[SerializeField] protected Transform cam;
		[SerializeField] protected Transform pivot;
		protected Vector3   lastTargetPosition;

		public Camera MainCam;


		/// <summary>
		/// Awake this instance.
		/// </summary>
		protected virtual void Awake()
		{
			cam     = GetComponentInChildren<Camera>().transform;
			pivot   = cam.parent;
			MainCam = cam.GetComponent<Camera> ();
		}


		/// <summary>
		/// Start this instance.
		/// </summary>
		protected override void Start () 
		{
			base.Start();
		}


		/// <summary>
		/// Update this instance.
		/// </summary>
		protected virtual void Update () 
		{

			if(!Application.isPlaying)
			{
				if(target != null)
				{
					Follow(999);
					lastTargetPosition = target.position;
				}

				if(Mathf.Abs(cam.localPosition.x) > .5f || Mathf.Abs(cam.localPosition.y) > .5f)
				{
					cam.localPosition = Vector3.Scale(cam.localPosition, Vector3.forward);
				}

				cam.localPosition = Vector3.Scale(cam.localPosition, Vector3.forward);
			}
		}


		/// <summary>
		/// Follow the specified deltaTime.
		/// </summary>
		/// <param name="deltaTime">Delta time.</param>
		protected override void Follow(float deltaTime) {}


		/// <summary>
		/// EventSystemのGameObjectにマウスオーバーしているか？ 
		/// </summary>
		/// <returns><c>true</c> if this instance is pointer over game object; otherwise, <c>false</c>.</returns>
		protected virtual bool IsPointerOverGameObject ()
		{
			EventSystem current = EventSystem.current;
			var hasCurrent = false;
			if (current != null) 
			{
				if (current.IsPointerOverGameObject()) 
				{
					hasCurrent = true; 
				}
				Touch[] _touchList = Input.touches;
				foreach (Touch t in _touchList) 
				{ 
					if (current.IsPointerOverGameObject(t.fingerId)) 
					{ 
						hasCurrent = true; 
					} 
				} 
			}
			return hasCurrent;
		}


		/// <summary>
		/// Determines whether this instance can screen touches.
		/// </summary>
		/// <returns><c>true</c> if this instance can screen touches; otherwise, <c>false</c>.</returns>
		protected virtual bool CanScreenTouches ()
		{
			//world time 0だと停止中 
			//float.Epsilon 限りなく小さい float 値
			//base.IsPointerOverGameObject -> ugui上を触れてるかどうか
			return (Time.timeScale < float.Epsilon || IsPointerOverGameObject ());
		}

	}



}

