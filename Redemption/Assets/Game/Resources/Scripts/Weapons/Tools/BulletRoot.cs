﻿using UnityEngine;
using System.Collections;
using Product;
using UniRx;
using UniRx.Triggers;
using System.Linq;



namespace Product.Weapons.Tools
{
	/// <summary>
	/// Bullet root.
	/// Charater1体に1つ付与
	/// WeaponBaseが座標を参照する
	/// </summary>
	public class BulletRoot : MonoBehaviour
	{
		const string EFFECT_NAME = "Bullet_Effect";

		/// <summary>
		/// The weapon.
		/// Bullet_Point
		/// </summary>
		public Transform Muzzle {
			get;
			set;
		}


		/// <summary>
		/// Gets or sets the particle.
		/// </summary>
		/// <value>The particle.</value>
		public ParticleSystem Particle {
			get;
			set;
		}
			

		/// <summary>
		/// Gets or sets the transform cache.
		/// </summary>
		/// <value>The transform cache.</value>
		public Transform TransformCache {
			get;
			set;
		}

		[SerializeField] Vector3 _offset;


		/// <summary>
		/// Start this instance.
		/// </summary>
		private void Start () 
		{
			Particle       = transform.FindChild (EFFECT_NAME).GetComponent<ParticleSystem> ();
			TransformCache = transform;

			//EveryGameObjectUpdateにしないとtransformがめちゃめちゃになる
			IObservable<long> shotStream = Observable.EveryGameObjectUpdate ();
			shotStream
				.Where(_ => Muzzle != null)
				.Subscribe (_ => {
					_UpdateStream ();
				}).AddTo (this);
		}


		/// <summary>
		/// Updates the transform.
		/// </summary>
		private void _UpdateStream ()
		{
			TransformCache.position = Muzzle.TransformPoint (Vector3.zero);
			TransformCache.forward  = Muzzle.TransformDirection (_offset);
		}


		/// <summary>
		/// Shot this instance.
		/// </summary>
		public void Shot ()
		{
			Particle.Emit (1);
		}
			

		/// <summary>
		/// Raises the draw gizmos event.
		/// </summary>
		void OnDrawGizmos ()
		{
			/*
			if (TransformCache != null && Particle != null) 
			{
				Gizmos.color = Color.magenta;
				Gizmos.DrawWireSphere (TransformCache.position, 0.1f);
			}
			*/
		}


	}


}

