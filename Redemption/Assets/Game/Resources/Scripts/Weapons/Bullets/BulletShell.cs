﻿using UnityEngine;
using System.Collections;
using Product;
using UniRx;
using UniRx.Triggers;
using System.Linq;


using Product.Character;

namespace Product.Weapons.Bullets
{
	/// <summary>
	/// Bullet shell.
	/// </summary>
	public class BulletShell : BulletBase 
	{

		string _myName;

		/// <summary>
		/// Start this instance.
		/// </summary>
		protected override void Start ()
		{
			base.Start();
			_myName = this.gameObject.name.ToString ();
		}


		/// <summary>
		/// Shot this instance.
		/// </summary>
		public override void Shot ()
		{
			//Debug.Log (TransformCache.forward);
			var point = base.TransformCache.forward + base.ShuftOffset;
			base.RigidBodyController.velocity = point * Power;
		}


		/// <summary>
		/// Raises the trigger enter event.
		/// </summary>
		/// <param name="col">Col.</param>
		protected override void OnCollisionEnter(Collision col) 
		{
			var _colSameBullet    = (col.gameObject.name == _myName);        //Bullet同士
			var _colStage         = col.gameObject.CompareTag (Tags.STAGE);  //Stageとか
			var _colWall          = col.gameObject.CompareTag (Tags.WALL);   //壁とか
			var _colMob           = col.gameObject.CompareTag (Tags.MOB);    //Mob *Enemyじゃない
			var _colPlayer        = col.gameObject.CompareTag (Tags.PLAYER); //Player
			//model
			CharacterModel _model = null;
			if (_colStage || _colWall || _colSameBullet || _colMob || _colPlayer)
			{
				base.OnColliderGroundNormal ();
			}

			if (_colMob || _colPlayer) 
			{
				_model = col.gameObject.GetComponent<CharacterModel> ();
			}
		}

		

	}
		

}
