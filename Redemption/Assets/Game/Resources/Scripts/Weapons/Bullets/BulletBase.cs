﻿using UnityEngine;
using System.Collections;
using Product;
using UniRx;
using UniRx.Triggers;
using System.Linq;



namespace Product.Weapons.Bullets
{
	/// <summary>
	/// Bullet base.
	/// </summary>
	public abstract class BulletBase : MonoBehaviour 
	{
		const string TRAIL = "Trail";

		/// <summary>
		/// Gets or sets the rigid body controller.
		/// </summary>
		/// <value>The rigid body controller.</value>
		public Rigidbody RigidBodyController {
			get;
			set;
		}


		/// <summary>
		/// Gets or sets the collider controller.
		/// </summary>
		/// <value>The collider controller.</value>
		public CapsuleCollider ColliderController {
			get;
			set;
		}


		/// <summary>
		/// Gets or sets the transform cache.
		/// </summary>
		/// <value>The transform cache.</value>
		public Transform TransformCache {
			get;
			set;
		}


		/// <summary>
		/// Gets or sets the trail ren.
		/// </summary>
		/// <value>The trail ren.</value>
		public TrailRenderer TrailRen {
			get;
			set;
		}


		/// <summary>
		/// The m life time.
		/// </summary>
		[SerializeField] protected float LifeTime = 4f;
		/// <summary>
		/// The hit effect prefab.
		/// </summary>
		[SerializeField] protected GameObject HitEffectPrefab;
		[SerializeField] protected Vector3 ShuftOffset;//new Vector3(0f, 0f, 0.2f);

		/// <summary>
		/// Gets or sets the power.
		/// </summary>
		/// <value>The power.</value>
		public float Power {
			get;
			set;
		}


		/// <summary>
		/// Start this instance.
		/// </summary>
		protected virtual void Awake ()
		{
			RigidBodyController = GetComponent<Rigidbody> ();
			ColliderController  = GetComponent<CapsuleCollider> ();
			TransformCache      = transform;
			{
				var tr   = TransformCache.FindChild (TRAIL);
				TrailRen = tr.GetComponent<TrailRenderer> () as TrailRenderer;
			}
		}


		/// <summary>
		/// Start this instance.
		/// </summary>
		protected virtual void Start ()
		{
			IObservable<long> lifeTimeStream = Observable.Timer (System.TimeSpan.FromSeconds (LifeTime));

			lifeTimeStream
				.Subscribe (_ => {
					Destroy (gameObject);
				})
				.AddTo (this);
		}


		/// <summary>
		/// Sets the position.
		/// </summary>
		/// <param name="point">Point.</param>
		public virtual void SetPosition (Vector3 point) 
		{
			TransformCache.position = point;
		}


		/// <summary>
		/// Sets the rotation.
		/// </summary>
		/// <param name="rotate">Rotate.</param>
		public virtual void SetRotation (Quaternion rotate)
		{
			TransformCache.rotation = rotate;
		}


		/// <summary>
		/// Shot this instance.
		/// </summary>
		public virtual void Shot ()
		{
			RigidBodyController.velocity = TransformCache.forward * Power;
		}


		/// <summary>
		/// Raises the collider ground normal event.
		/// </summary>
		protected virtual void OnColliderGroundNormal ()
		{
			RigidBodyController.constraints = RigidbodyConstraints.None;
			ColliderController.radius       = ColliderController.radius / 4f;
			TrailRen.startWidth = TrailRen.startWidth / 2f;
			TrailRen.endWidth   = TrailRen.endWidth / 2f;
			//Debug.Log (col.gameObject);
		}

		/// <summary>
		/// Raises the trigger enter event.
		/// </summary>
		/// <param name="col">Col.</param>
		protected virtual void OnTriggerEnter(Collider col) {}
		/// <summary>
		/// Raises the trigger exit event.
		/// </summary>
		/// <param name="col">Col.</param>
		protected virtual void OnTriggerExit(Collider col) {}
		/// <summary>
		/// Raises the trigger stay event.
		/// </summary>
		/// <param name="col">Col.</param>
		protected virtual void OnTriggerStay(Collider col) {}
		/// <summary>
		/// Raises the collision enter event.
		/// </summary>
		/// <param name="col">Col.</param>
		protected virtual void OnCollisionEnter(Collision col) {}
		/// <summary>
		/// Raises the collision exit event.
		/// </summary>
		/// <param name="col">Col.</param>
		protected virtual void OnCollisionExit(Collision col) {}
		/// <summary>
		/// Raises the collision stay event.
		/// </summary>
		/// <param name="col">Col.</param>
		protected virtual void OnCollisionStay(Collision col) {}



	}


}
