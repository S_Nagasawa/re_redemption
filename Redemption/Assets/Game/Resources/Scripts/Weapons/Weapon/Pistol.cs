﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;


namespace Product.Weapons.Weapon
{
	/// <summary>
	/// WeaponBaseの派生
	/// Pistol.
	/// Bone Handの子にする
	/// </summary>
	public class Pistol : WeaponBase
	{

		const string Action = "ShotAction";
		[SerializeField] private GameObject m_MuzzleFire;


		/// <summary>
		/// Start this instance.
		/// </summary>
		protected override void Start () 
		{
			base.Start ();
			base.m_EffectCallback += (() => {
				_SetMuzzleVisible(false);
			});
		}


		/// <summary>
		/// Raises the shot event.
		/// </summary>
		protected override void OnShot ()
		{
			base.AnimatorController.SetTrigger (Action);
			AnimatorStateInfo info = base.AnimatorController.GetCurrentAnimatorStateInfo (0);
			base.CoolTime = info.length/2f;
			_SetMuzzleVisible ();
			base.OnShot ();
		}


		/// <summary>
		/// Sets the muzzle visible.
		/// </summary>
		/// <param name="visible">If set to <c>true</c> visible.</param>
		void _SetMuzzleVisible (bool visible = true)
		{
			if (m_MuzzleFire != null) 
			{
				m_MuzzleFire.SetActive (visible);
			}
		}


	}




}

