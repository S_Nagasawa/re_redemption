﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;

namespace Product.Weapons.Weapon
{
	/// <summary>
	/// WeaponBaseの派生
	/// Pistol.
	/// Bone Handの子にする
	/// </summary>
	public class AssaultRifle : WeaponBase
	{

		//@TODO
		Vector3 _rotateVec3 = new Vector3 (-78.3537f, -275.2584f, 242.3004f);

		/// <summary>
		/// Awake this instance.
		/// </summary>
		protected override void Awake ()
		{
			base.m_RotationVec3 = _rotateVec3;
			base.Awake ();
		}


		/// <summary>
		/// Start this instance.
		/// </summary>
		protected override void Start () 
		{
			base.Start ();

			base.m_EffectCallback += (() => 
				{
					//do something
				}
			);

			base.transformStream = Observable.EveryGameObjectUpdate ();

			base.transformStream
				.Where (_ => base.IsEmptyTarget() && base.IsTargeting)
				.Subscribe (_ => {
					ResetRotate();
				}).AddTo (this);
		}


		/// <summary>
		/// Resets the rotate.
		/// </summary>
		public override void ResetRotate ()
		{
			TransformCache.rotation = Quaternion.Euler(base.m_RotationVec3);
		}

	}




}

