﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;

using Product.Weapons.Tools;
using Product.Weapons.Bullets;

namespace Product.Weapons.Weapon
{

	/// <summary>
	/// I bullets handler.
	/// </summary>
	public interface IBulletsHandler
	{
		bool IsBulletEmpty ();
		bool IsBulletHalf ();
	}


	/// <summary>
	/// Weapon base.
	/// 武器の基底クラス
	/// </summary>
	public abstract class WeaponBase : MonoBehaviour, IBulletsHandler
	{
		#region Target
		/// <summary>
		/// Gets or sets the target.
		/// </summary>
		/// <value>The target.</value>
		public Transform Target { get { return m_Target; } set { m_Target = value; } }
		private Transform m_Target;
		#endregion

		/// <summary>
		/// The BULLE POINT NAME.
		/// Transform child Name
		/// </summary>
		static readonly string BULLET_POINT_NAME = "Bullet_Point";


		protected Animator AnimatorController;

		public Transform TransformCache { get; set; }
		public Transform BulletPointTransform { get; set; }
		public BoxCollider ColliderController { get; set; }
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the bullet.
		/// 残数
		/// </summary>
		/// <value>The bullet.</value>
		[SerializeField] private int m_BulletNum;
		public int  BulletNum { get { return m_BulletNum; } set { m_BulletNum = value; } }
		public int  MaxBullet;

		/// <summary>
		/// The attack rate.
		/// 威力
		/// </summary>
		public int AttackRate { get; set; }

		/// <summary>
		/// Gets or sets the durability.
		/// 耐久度
		/// </summary>
		/// <value>The durability.</value>
		public int Durability { get; set; }

		/// <summary>
		/// Gets or sets the root.
		/// </summary>
		/// <value>The root.</value>
		public BulletRoot Root { get { return m_Root; } set { m_Root = value; } }
		[SerializeField] private BulletRoot m_Root;

		/// <summary>
		/// Gets or sets the power.
		/// </summary>
		/// <value>The power.</value>
		public float Power = 30f;
		/// <summary>
		/// The cool time.
		/// </summary>
		public float CoolTime { get { return m_CoolTime; } set { m_CoolTime = value; } }
		/// <summary>
		/// The shell prefab.
		/// </summary>
		public GameObject ShellPrefab;
		[SerializeField] private float m_CoolTime;
		private bool  m_CoolDown;

		protected Vector3 m_RotationVec3;
		protected IObservable<long> transformStream;


		/// <summary>
		/// Gets or sets a value indicating whether this instance is targeting.
		/// </summary>
		/// <value><c>true</c> if this instance is targeting; otherwise, <c>false</c>.</value>
		public bool IsTargeting { get; set; }

		/// <summary>
		/// Determines whether this instance is bullet none.
		/// </summary>
		/// <returns><c>true</c> if this instance is bullet none; otherwise, <c>false</c>.</returns>
		public bool IsBulletEmpty ()
		{
			return (BulletNum <= 0);
		}
			
		/// <summary>
		/// Determines whether this instance is bullet half.
		/// </summary>
		/// <returns><c>true</c> if this instance is bullet half; otherwise, <c>false</c>.</returns>
		public bool IsBulletHalf ()
		{
			return (BulletNum <= (float)BulletNum / 2);
		}


		/// <summary>
		/// The effect callback.
		/// </summary>
		protected System.Action m_EffectCallback = delegate {};

		/// <summary>
		/// Awake this instance.
		/// </summary>
		protected virtual void Awake ()
		{
			TransformCache       = transform;
			BulletPointTransform = TransformCache.FindChild (BULLET_POINT_NAME);
			ColliderController   = GetComponent<BoxCollider> () as BoxCollider;
			AnimatorController   = GetComponent<Animator> () as Animator;
		}


		/// <summary>
		/// Start this instance.
		/// </summary>
		protected virtual void Start () 
		{
			IObservable<long> colliderStream  = Observable.EveryUpdate ();
			IObservable<long> transformStream = Observable.EveryGameObjectUpdate ();

			colliderStream
				.Subscribe (_ => {
					ColliderHandle ();
				})
				.AddTo (this);


			transformStream
				.Where(_ => !IsEmptyTarget())
				.Subscribe (_ => {
					TransformCache.LookAt(Target.position);
				})
				.AddTo (this);
		}


		/// <summary>
		/// Shot this instance.
		/// </summary>
		public virtual void Shot () 
		{
			if (CanShot ()) 
			{
				BulletNum--;
				OnShot ();
			}
		}


		/// <summary>
		/// Determines whether this instance is empty target.
		/// </summary>
		/// <returns><c>true</c> if this instance is empty target; otherwise, <c>false</c>.</returns>
		protected virtual bool IsEmptyTarget ()
		{
			return Target == null;
		}


		/// <summary>
		/// Determines whether this instance can shot.
		/// 弾数が空じゃないか、装填中じゃないか、prefabがnullじゃないか、rootがnullじゃないか
		/// </summary>
		/// <returns><c>true</c> if this instance can shot; otherwise, <c>false</c>.</returns>
		public virtual bool CanShot ()
		{
			return !IsBulletEmpty () && !m_CoolDown && ShellPrefab != null && Root != null;
		}


		/// <summary>
		/// Raises the shot event.
		/// </summary>
		protected virtual void OnShot ()
		{
			BulletBase _bullet = CreateShells<BulletBase>();
			if (_bullet == null)
				return;

			var tr = Root.TransformCache;
			if (_bullet is BulletShell) 
			{
				BulletShell _shell = (BulletShell)_bullet;
				_shell.SetPosition (tr.position);
				_shell.SetRotation (tr.rotation);
				_shell.Power = Power;
				_shell.Shot ();
			}
			StartCoroutine (CoolDownWait ());
		}


		/// <summary>
		/// Cools down wait.
		/// </summary>
		/// <returns>The down wait.</returns>
		protected virtual IEnumerator CoolDownWait ()
		{
			m_CoolDown = true;
			float _t   = 0.0f;
			while (_t < CoolTime) 
			{
				_t += Time.deltaTime;
				yield return null;
			}
			if (m_EffectCallback != null) 
			{
				m_EffectCallback ();
			}
			m_CoolDown = false;
			yield return null;
		}
			

		/// <summary>
		/// Creates the shells.
		/// </summary>
		/// <returns>The shells.</returns>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		protected virtual T CreateShells<T> () where T :Component
		{
			var go = (GameObject)Instantiate (ShellPrefab, Vector3.zero, Quaternion.identity);
			go.name = ShellPrefab.name;
			if (BulletController.Instance.TransformCache != null) 
			{
				go.transform.SetParent (BulletController.Instance.TransformCache);				
			}
			return go.GetComponent<T> ();
		}


		/// <summary>
		/// Colliders the handle.
		/// </summary>
		protected virtual void ColliderHandle ()
		{
			if (ColliderController == null)
				return;	
			ColliderController.enabled = !m_CoolDown;
		}


		/// <summary>
		/// Resets the rotate.
		/// </summary>
		public virtual void ResetRotate ()
		{
			TransformCache.rotation = Quaternion.Euler(m_RotationVec3);
		}
			


	}




}

