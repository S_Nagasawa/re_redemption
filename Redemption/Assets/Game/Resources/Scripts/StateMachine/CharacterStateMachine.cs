﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


using Product;

namespace Product.StateMachine
{
	/// <summary>
	/// Character state machine.
	/// </summary>
	public class CharacterStateMachine : StateMachineBehaviour 
	{

		Animator Target;
		//特定のステートのみ実行されるコルーチン
		private IEnumerator m_Coroutine;
		private MonoBehaviour m_Mono;

		/// <summary>
		/// 新しいステートに移り変わった時に実行
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			m_Coroutine = Coroutine ();

			if (m_Mono == null)
				m_Mono = animator.GetComponent<MonoBehaviour> () as MonoBehaviour;
		}


		/// <summary>
		/// ステートが次のステートに移り変わる直前に実行
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (m_Mono != null)
				m_Mono.StopCoroutine (m_Coroutine);
		}


		/// <summary>
		/// スクリプトが貼り付けられたステートマシンに遷移してきた時に実行
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateMachinePathHash">State machine path hash.</param>
		public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
		{
		}


		/// <summary>
		/// スクリプトが貼り付けられたステートマシンから出て行く時に実行
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateMachinePathHash">State machine path hash.</param>
		public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
		{
		}


		/// <summary>
		/// MonoBehaviour.OnAnimatorMoveの直後に実行される
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}


		/// <summary>
		/// 最初と最後のフレームを除く、各フレーム単位で実行
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}


		/// <summary>
		/// MonoBehaviour.OnAnimatorIKの直後に実行される
		/// </summary>
		/// <param name="animator">Animator.</param>
		/// <param name="stateInfo">State info.</param>
		/// <param name="layerIndex">Layer index.</param>
		public override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}


		IEnumerator Coroutine ()
		{
			while (true) 
			{
				Debug.Log ("Call");
				yield return new WaitForEndOfFrame ();
			}
		}

	}

}

