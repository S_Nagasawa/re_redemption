﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using System.Linq;
using UnityEngine.UI;


namespace Product.UI
{

	/// <summary>
	/// Menu UI
	/// </summary>
	public class MenuUI : MonoBehaviour 
	{
		private static readonly string MENU_TOGGLE  = "Menu_Toggle";
		private static readonly string MENU_OVERLAY = "Menu_Overlay";
		private static readonly string MENU_CONTENT = "Menu_Content";

		const string SHOW = "Show";
		const string HIDE = "Hide";

		private float  m_TimeScaleRef = 1f;
		private float  m_VolumeRef    = 1f;
		private Transform TransformCache;
		private Animator  m_ContentAnim;

		[SerializeField] private bool   m_Paused;
		[SerializeField] private Toggle m_MenuToggle;
		[SerializeField] private Image  m_Overlay;
		[SerializeField] private Image  m_Content;
		[SerializeField] float _delayTime = 0.5f;


		/// <summary>
		/// Awake this instance.
		/// </summary>
		private void Awake ()
		{
			TransformCache = transform;
			m_MenuToggle   = TransformCache.FindChild (MenuUI.MENU_TOGGLE).GetComponent<Toggle> () as Toggle;
			m_Overlay      = TransformCache.FindChild (MenuUI.MENU_OVERLAY).GetComponent<Image> () as Image;
			m_Content      = TransformCache.FindChild (MenuUI.MENU_CONTENT).GetComponent<Image> () as Image;
			m_ContentAnim  = m_Content.GetComponent<Animator> () as Animator;
		}


		/// <summary>
		/// Start this instance.
		/// </summary>
		private void Start ()
		{
			m_MenuToggle
				.OnValueChangedAsObservable ()
				.Subscribe (_ => {
					OnMenuStateChange ();
				});
		}


		/// <summary>
		/// Menus the on.
		/// </summary>
		private void _MenuOn ()
		{
			m_TimeScaleRef       = Time.timeScale;
			m_VolumeRef          = AudioListener.volume;
			Time.timeScale       = 0f;
			AudioListener.volume = 0f;
			m_Paused             = true;
			m_Overlay.gameObject.SetActive (m_Paused);
			m_Content.gameObject.SetActive (m_Paused);
			m_ContentAnim.SetTrigger (SHOW);
		}


		/// <summary>
		/// Menus the off.
		/// </summary>
		private void _MenuOff ()
		{
			Time.timeScale       = m_TimeScaleRef;
			AudioListener.volume = m_VolumeRef;
			m_Paused             = false;
			m_Overlay.gameObject.SetActive (m_Paused);
			m_ContentAnim.SetTrigger (HIDE);
			StartCoroutine (_Reset (() => {
				m_Content.gameObject.SetActive (m_Paused);
			}));
		}


		/// <summary>
		/// Reset the specified callback.
		/// </summary>
		/// <param name="callback">Callback.</param>
		IEnumerator _Reset (System.Action callback = null)
		{
			AnimatorStateInfo info = m_ContentAnim.GetCurrentAnimatorStateInfo (0);
			float _t    = 0.0f;
			float _time = info.length + _delayTime;
			while (_t < _time)
			{
				_t += Time.deltaTime;
				yield return null;
			}
			if (callback != null) 
			{
				callback ();
			}
			yield return null;
		}


		/// <summary>
		/// Raises the menu status change event.
		/// </summary>
		public void OnMenuStateChange ()
		{
			if (m_MenuToggle == null)
				return;

			if (m_MenuToggle.isOn && !m_Paused)
			{
				_MenuOn();
			}
			else if (!m_MenuToggle.isOn && m_Paused)
			{
				_MenuOff();
			}
		}



	}


}
