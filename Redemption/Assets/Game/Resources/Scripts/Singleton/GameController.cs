﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


using Product.Tools;
using Product.Systems;
using Product.Character;

namespace Product
{
	/// <summary>
	/// Game controller.
	/// </summary>
	public class GameController : Singleton<GameController>
	{
		/// <summary>
		/// Stage state.
		/// </summary>
		public enum StageState
		{
			INTRO,		//始まり
			START,		//開始
			SCENARIO,	//シナリオ
			GAMEOVER,	//GameOver
			RESULT,		//クリア
		}
		/// <summary>
		/// Characterを格納するEmpty GameObject
		/// </summary>
		[SerializeField] private Transform m_CharacterRoot;
		/// <summary>
		/// The m player prefab. Player Prefab
		/// </summary>
		[SerializeField] private GameObject m_PlayerPrefab;
		/// <summary>
		/// player.
		/// </summary>
		[SerializeField] private PlayerCharacter m_Player;
		/// <summary>
		/// singleton state
		/// </summary>
		[SerializeField] private StageState m_State;
		/// <summary>
		/// initialize position
		/// </summary>
		[SerializeField] private Vector3 m_PlayerStartPosition;



		/// <summary>
		/// Gets a value indicating whether this instance is game over.
		/// </summary>
		/// <value><c>true</c> if this instance is game over; otherwise, <c>false</c>.</value>
		public bool IsGameOver
		{
			get {
				return m_State == StageState.GAMEOVER;
			}
		}

		protected override void Awake ()
		{
			base.Awake ();
			m_Player = _GetPlayer ();
			m_State  = StageState.INTRO;
		}

		void Start () {}
		void Update () {}

		void FixedUpdate ()
		{
			if (m_Player != null && m_Player.Status.IsDie ()) 
			{
				m_State = StageState.GAMEOVER;
			}
		}

		/// <summary>
		/// Gets the player.
		/// Sceneに配置してればそれを返却、なければResourcesからLoadする
		/// </summary>
		/// <returns>The player.</returns>
		private PlayerCharacter _GetPlayer () 
		{
			PlayerCharacter _p = null;
			if (m_PlayerPrefab == null) 
			{
				var go = GameObject.FindGameObjectWithTag (Product.Tags.PLAYER);
				if (go == null) 
				{
					_p = Owner.Generate<PlayerCharacter> (m_PlayerPrefab, m_PlayerStartPosition, (m_CharacterRoot == null) ? base.TransformCache : m_CharacterRoot);
				} 
				else
				{
					_p = go.GetComponent<PlayerCharacter> () as PlayerCharacter;					
				}
			}
			return _p;
		}


		#region Editor
		protected override void OnDrawGizmos () 
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(m_PlayerStartPosition, .5f);
		}
		#endregion

	}


}

