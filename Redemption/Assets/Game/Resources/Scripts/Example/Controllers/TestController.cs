﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Product;
using UniRx;
using UniRx.Triggers;

using Product.Tools;
using Product.Systems;



namespace Product.Example.Controllers
{

	/// <summary>
	/// Test controller.
	/// </summary>
	public class TestController : Singleton<TestController>
	{

		List<ITestModel> testModel;
		Dictionary<int, ITestModel> testDic;

		protected override void Awake () 
		{
			base.Awake ();
		}

		private void Start ()
		{
			testModel = new List<ITestModel> ();
			testDic   = new Dictionary<int, ITestModel> ();

			{
				TestModel model = new TestModel(null, 1, "hoge");
				testModel.Add (model);
			}

			{
				TestModel model = new TestModel(null, 2, "fuga");
				testModel.Add(model);
			}

			{
				TestModel model = new TestModel(null, 3, "piyo");
				testModel.Add(model);
			}

			{
				TestModel model = new TestModel(null, 30, "piyo-piyo");
				testModel.Add(model);
			}

			{
				TestModel model = new TestModel(null, 62, "hoge-hoge");
				testModel.Add(model);
			}

			for (int i = 0; i < testModel.Count; ++i) 
			{
				testDic.Add (i, testModel [i]);
			}

			testModel.ForEach ((delegate(ITestModel obj) 
				{
					//Debug.Log(obj.Name);
				}
			));

			foreach (ITestModel model in testModel) 
			{
				//Debug.Log ("List=>" + model.Name);
			}

			foreach (ITestModel model in testDic.Values) 
			{
				//Debug.Log ("Dictionary=>" + model.ID);
			}
		}

	}


	public interface ITestModel
	{
		Transform Tr { get; }
		uint ID { get; }
		string Name { get; }
	}


	public class TestModel : ITestModel
	{
		public Transform Tr { get; private set; }
		public uint ID { get; private set; }
		public string Name { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Product.TestModel"/> class.
		/// </summary>
		/// <param name="tr">Tr.</param>
		/// <param name="ids">Identifiers.</param>
		/// <param name="name">Name.</param>
		public TestModel (Transform tr = null, uint ids = 0, string name = "")
		{
			Tr   = tr;
			ID   = ids;
			Name = name;
		}
	}


}

