﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using System.Linq;
using System.IO;

namespace Product
{

	/// <summary>
	/// Capture creater.
	/// </summary>
	public class CaptureCreater : EditorWindow
	{
		// このディレクトリ以下のprefabのキャプチャを全て取得
		UnityEngine.Object searchDirectory;
		List<GameObject> objList = new List<GameObject>();
		string dirPath = "Assets/Game/Captures/"; // 出力先ディレクトリ(Assets/Game/Captures/以下に出力)
		int width      = 240; // キャプチャ画像の幅
		int height     = 240; // キャプチャ画像の高さ
		int size       = 120; // Label幅
		Vector2 buttonSize = new Vector2(120, 44 );

		//サムネイルサイズ
		int texSize    = 80;
		List<Texture2D> texList = new List<Texture2D>();
		bool showThumbNail = false;

		[MenuItem("Redemption/CaptureCreater")]
		static void ShowWindow ()
		{
			EditorWindow.GetWindow (typeof(CaptureCreater));
		}


		/// <summary>
		/// Raises the GU event.
		/// </summary>
		void OnGUI ()
		{
			EditorGUILayout.Space();
			_OnGUIBase ();
			_OnCaptureGUI ();
			if (showThumbNail) 
				_OnTextureGUI ();
			EditorGUILayout.Space();
		}


		/// <summary>
		/// Ons the GUI base.
		/// </summary>
		void _OnGUIBase()
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("Search Directory : ", GUILayout.Width(size));
			searchDirectory = EditorGUILayout.ObjectField(searchDirectory, typeof(UnityEngine.Object), true);
			GUILayout.EndHorizontal();
			EditorGUILayout.Space();

			GUILayout.BeginHorizontal();
			GUILayout.Label("Save directory : ", GUILayout.Width(size));
			dirPath = (string)EditorGUILayout.TextField(dirPath);
			GUILayout.EndHorizontal();
			EditorGUILayout.Space();

			GUILayout.BeginHorizontal();
			GUILayout.Label("Width : ", GUILayout.Width(size));
			width = EditorGUILayout.IntField(width);
			GUILayout.EndHorizontal();
			EditorGUILayout.Space();

			GUILayout.BeginHorizontal();
			GUILayout.Label("Height : ", GUILayout.Width(size));
			height = EditorGUILayout.IntField(height);
			GUILayout.EndHorizontal();
			EditorGUILayout.Space();

		}


		/// <summary>
		/// Ons the capture GU.
		/// </summary>
		void _OnCaptureGUI ()
		{
			GUILayout.BeginHorizontal();
			if(GUILayout.Button ( "Capture", GUILayout.Width( buttonSize.x ),GUILayout.Height( buttonSize.y )))
			{
				if (searchDirectory == null) 
				{
					return;
				}
				// 出力先ディレクトリを生成
				if (!System.IO.File.Exists(dirPath))
				{
					System.IO.Directory.CreateDirectory(dirPath);
				}
				objList.Clear();
				texList.Clear ();

				// 指定ディレクトリ内のprefabを全て取り出してListに入れる
				string   replaceDirectoryPath = AssetDatabase.GetAssetPath(searchDirectory);
				string[] filePaths            = Directory.GetFiles( replaceDirectoryPath , "*.*" );
				foreach(string filePath in filePaths)
				{
					GameObject obj =  AssetDatabase.LoadAssetAtPath( filePath , typeof(GameObject)) as GameObject;
					if(obj != null)
					{
						objList.Add(obj);
					}
				}
				EditorCoroutine.Start(Exec(objList));
			}
			GUILayout.EndHorizontal();
			EditorGUILayout.Space();
		}
			


		/// <summary>
		/// Exec the specified objList.
		/// List内のGameObjectを配置しつつ、キャプチャを取得
		/// </summary>
		/// <param name="objList">Object list.</param>
		IEnumerator Exec(List<GameObject> objList)
		{
			foreach(GameObject obj in objList)
			{
				// Instantiateして向きを調整して取りやすい位置に
				GameObject unit = Instantiate(obj , Vector3.zero , Quaternion.identity) as GameObject;
				unit.transform.eulerAngles = new Vector3(270.0f , 0.0f , 0.0f);

				yield return new EditorCoroutine.WaitForSeconds(1.0f);
				Capture(obj.name);
				// キャプチャ撮った後は捨てる
				DestroyImmediate(unit);
			}
			showThumbNail = true;
		}


		/// <summary>
		/// Ons the texture GU.
		/// </summary>
		void _OnTextureGUI()
		{
			GUILayout.BeginHorizontal();

			var options = new []
			{
				GUILayout.Width (texSize), 
				GUILayout.Height (texSize)
			};

			for (int i = 0; i < texList.Count; i++) 
			{
				Texture2D t = texList[i];
				if (i % 5 == 0) 
				{
					GUILayout.EndHorizontal ();
					GUILayout.BeginHorizontal ();
				}
				EditorGUILayout.ObjectField (t, typeof(Texture2D), false, options);
			}
			GUILayout.EndHorizontal ();
		}


		/// <summary>
		/// Capture the specified fileName.
		/// </summary>
		/// <param name="fileName">File name.</param>
		void Capture(string fileName)
		{
			Vector3 nowPos  = Camera.main.transform.position;
			float nowSize   = Camera.main.orthographicSize;

			Camera.main.transform.position  = new Vector3 (nowPos.x, nowPos.y, nowPos.z);
			Camera.main.orthographicSize    = 100.0f;
			RenderTexture renderTexture     = new RenderTexture(width, height, 24);// RenderTextureを生成して、これに現在のSceneに映っているものを書き込む
			Camera.main.targetTexture       = renderTexture;

			Camera.main.Render();
			RenderTexture.active = renderTexture;
			Texture2D texture2D  = new Texture2D(width, height, TextureFormat.ARGB32, false);
			texture2D.ReadPixels( new Rect(0, 0, width, height), 0, 0);
			Camera.main.targetTexture = null;

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					Color c = texture2D.GetPixel(x,y);
					c       = new Color(c.r , c.g , c.b , c.a);
					texture2D.SetPixel(x , y , c);
				}
			}

			// textureのbyteをファイルに出力
			byte[] bytes = texture2D.EncodeToPNG();
			var path     = dirPath + fileName + ".png";
			System.IO.File.WriteAllBytes(path, bytes );


			//load textures
			Texture2D tex = EditorGUIUtility.Load (path) as Texture2D;
			texList.Add (tex);


			// 後処理
			Camera.main.targetTexture = null;
			RenderTexture.active      = null;
			renderTexture.Release();
			Camera.main.transform.position = nowPos;// カメラを元に戻す
			Camera.main.orthographicSize   = nowSize;
			Resources.UnloadUnusedAssets();
			System.GC.Collect();
		}
			

		/// <summary>
		/// Reads the png.
		/// </summary>
		/// <returns>The png.</returns>
		/// <param name="path">Path.</param>
		Texture2D ReadPng (string path)
		{
			byte[] readBinary = ReadPngFile (path);
			int pos = 16;//16バイト

			int width = 0;
			for (int i = 0; i < 4; ++i) 
			{
				width = width * 256 + readBinary [pos++];
			}
			int height = 0;
			for (int i = 0; i < 4; i++)
			{
				height = height * 256 + readBinary[pos++];
			}

			Texture2D texture = new Texture2D(width, height);
			texture.LoadImage(readBinary);

			return texture;
		}


		/// <summary>
		/// Reads the png file.
		/// </summary>
		/// <returns>The png file.</returns>
		/// <param name="path">Path.</param>
		byte[] ReadPngFile (string path)
		{
			FileStream fileStream = new FileStream (path, FileMode.Open, FileAccess.Read);
			BinaryReader bin      = new BinaryReader (fileStream);
			byte[] value = bin.ReadBytes ((int)bin.BaseStream.Length);

			bin.Close ();
			return value;
		}

	}

}

