﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Product;
using UniRx;
using UniRx.Triggers;
using System.Linq;

/// <summary>
/// Mesh counter.
/// Inspectorにアクセスして、mesh数をカウントするプラグイン
/// </summary>
namespace Product 
{
	/// <summary>
	/// Mesh counter.
	/// </summary>
	[CustomEditor(typeof(MeshFilter))]
	public class MeshCounter : Editor
	{
		/// <summary>
		/// Raises the inspector GU event.
		/// </summary>
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI ();
			MeshFilter fill = target as MeshFilter;
			var polygons    = "MeshFilterCount: " + fill.sharedMesh.triangles.Length / 3;
			EditorGUILayout.LabelField (polygons);
		}
	}


	/// <summary>
	/// Skin mesh counter.
	/// </summary>
	[CustomEditor(typeof(SkinnedMeshRenderer))]
	public class SkinMeshCounter : Editor
	{
		/// <summary>
		/// Raises the inspector GU event.
		/// </summary>
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI ();
			SkinnedMeshRenderer skin = target as SkinnedMeshRenderer;
			var polygons             = "SkinMeshCount: " + skin.sharedMesh.triangles.Length / 3;
			EditorGUILayout.LabelField (polygons);
		}
	}



}

