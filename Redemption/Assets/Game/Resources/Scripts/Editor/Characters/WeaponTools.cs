﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using System.Linq;



namespace Product.Weapons.Weapon
{
	
	[CustomEditor(typeof (WeaponBase), true)]
	[CanEditMultipleObjects]
	public class WeaponTools : Editor
	{
		protected WeaponBase weapon = null;

		public override void OnInspectorGUI () 
		{
			base.OnInspectorGUI ();

			weapon = (WeaponBase)target;

			Transform parent = weapon.transform.parent;
			EditorGUILayout.ObjectField ("Parent Bone", parent, typeof(Transform), false);
		}
	}


}

