﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using System.Linq;

namespace Product
{
	/// <summary>
	/// エディター上でStartCorutineのような処理を使用可能にするクラス
	/// via http://qiita.com/k_yanase/items/686fc3134c363ffc5239
	/// </summary>	
	[InitializeOnLoad]
	public sealed class EditorCoroutine
	{
		/// <summary>
		/// Initializes the <see cref="Product.EditorCoroutine"/> class.
		/// </summary>
		static EditorCoroutine ()
		{
			EditorApplication.update += Update;
		}


		static Dictionary<IEnumerator, EditorCoroutine.Coroutine> asyncList = new Dictionary<IEnumerator, Coroutine>();
		static List<EditorCoroutine.WaitForSeconds> waitForSecondsList      = new List<EditorCoroutine.WaitForSeconds>();


		/// <summary>
		/// Update this instance.
		/// </summary>
		static void Update ()
		{
			CheackIEnumerator ();
			CheackWaitForSeconds ();
		}


		/// <summary>
		/// Cheacks the I enumerator.
		/// </summary>
		static void CheackIEnumerator ()
		{
			List<IEnumerator> removeList = new List<IEnumerator> ();
			foreach(KeyValuePair<IEnumerator, EditorCoroutine.Coroutine> pair in asyncList)
			{
				if(pair.Key != null)
				{
					//IEnumratorのCurrentがCoroutineを返しているかどうか 
					EditorCoroutine.Coroutine c = pair.Key.Current as EditorCoroutine.Coroutine;
					if(c != null)
					{
						if(c.isActive) continue;
					}
					//wwwクラスのダウンロードが終わっていなければ進まない 
					WWW www = pair.Key.Current as WWW;
					if(www != null)
					{
						if(!www.isDone) continue; 
					}
					//これ以上MoveNextできなければ終了 
					if(!pair.Key.MoveNext())
					{
						if(pair.Value != null)
						{
							pair.Value.isActive = false;
						}
						removeList.Add(pair.Key);
					}
				}
				else 
				{
					removeList.Add(pair.Key);
				}
			}

			foreach(IEnumerator async in removeList) 
			{
				asyncList.Remove(async);
			}
		}


		/// <summary>
		/// Cheacks the wait for seconds.
		/// </summary>
		static void CheackWaitForSeconds () 
		{
			for(int i = 0; i < waitForSecondsList.Count; i++)
			{
				if(waitForSecondsList[i] != null)
				{
					if(EditorApplication.timeSinceStartup - waitForSecondsList[i].InitTime > waitForSecondsList[i].Time)
					{
						waitForSecondsList[i].isActive = false;
						waitForSecondsList.RemoveAt(i);
					}
				}
				else 
				{
					Debug.LogError("rem");
					waitForSecondsList.RemoveAt(i);
				}
			}
		}


		/// <summary>
		/// Start the specified iEnumerator.
		/// </summary>
		/// <param name="iEnumerator">I enumerator.</param>
		public static EditorCoroutine.Coroutine Start (IEnumerator iEnumerator)
		{
			if(Application.isEditor && !Application.isPlaying)
			{
				EditorCoroutine.Coroutine c = new Coroutine();
				if(!asyncList.Keys.Contains(iEnumerator)) asyncList.Add(iEnumerator, c);
				iEnumerator.MoveNext();
				return c;
			}
			else 
			{
				Debug.LogError("EditorCoroutine.Startはゲーム起動中に使うことはできません");
				return null;
			}
		}

		/// <summary>
		/// コルーチンを停止します 
		/// </summary>
		public static void Stop (IEnumerator iEnumerator) 
		{
			if(Application.isEditor)
			{
				if(asyncList.Keys.Contains(iEnumerator))
				{
					asyncList.Remove(iEnumerator);
				}
			}
			else 
			{
				Debug.LogError("EditorCoroutine.Stopはゲーム中に使うことはできません");
			}
		}

		/// <summary>
		/// 使用不可
		/// WaitForSecondsのインスタンスを登録します 
		/// </summary>
		static public void AddWaitForSecondsList (EditorCoroutine.WaitForSeconds coroutine)
		{
			if(waitForSecondsList.Contains(coroutine) == false)
			{
				waitForSecondsList.Add(coroutine);
			}
		}

		/// <summary>
		/// Coroutine.
		/// </summary>
		public class Coroutine
		{
			public bool isActive;

			public Coroutine ()
			{
				isActive = true;
			}
		}


		/// <summary>
		/// Wait for seconds.
		/// </summary>
		public sealed class WaitForSeconds : EditorCoroutine.Coroutine 
		{
			private float time;
			private double initTime;

			public float  Time     { get{ return time; } }
			public double InitTime { get{ return initTime; } }

			public WaitForSeconds(float time) : base()
			{
				this.time = time;
				this.initTime = EditorApplication.timeSinceStartup;
				EditorCoroutine.AddWaitForSecondsList(this);
			}
		}


	}



}

