﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UniRx;
using UniRx.Triggers;
using System.Linq;


namespace Product.Example
{

	/// <summary>
	/// Preview example inspector.
	/// </summary>
	[CustomEditor(typeof(PreviewExample))]
	public class PreviewExampleInspector : Editor 
	{
		PreviewRenderUtility previewRenderUtility; 
		GameObject previewObject;


		/// <summary>
		/// Raises the enable event.
		/// </summary>
		void OnEnable ()
		{
			//true にすることでシーン内のゲームオブジェクトを描画できるようになる
			previewRenderUtility = new PreviewRenderUtility (true);

			//FieldOfView を 30 にするとちょうどいい見た目になる
			//必要に応じて nearClipPlane と farClipPlane を設定
			previewRenderUtility.m_CameraFieldOfView    = 30f;
			previewRenderUtility.m_Camera.nearClipPlane = 0.3f;
			previewRenderUtility.m_Camera.farClipPlane  = 1000;

			//コンポーネント経由でゲームオブジェクトを取得
			var component = (Component)target;
			previewObject = component.gameObject;
		}


		/// <summary>
		/// Raises the disable event.
		/// </summary>
		void OnDisable ()
		{
			previewRenderUtility.Cleanup ();
			previewRenderUtility = null;
			previewObject        = null;
		}


		/// <summary>
		/// Determines whether this instance has preview GU.
		/// Previewを有効にする
		/// </summary>
		/// <returns><c>true</c> if this instance has preview GU; otherwise, <c>false</c>.</returns>
		public override bool HasPreviewGUI () { return true; }


		/// <summary>
		/// Gets the preview title.
		/// Preview Title
		/// </summary>
		/// <returns>The preview title.</returns>
		public override GUIContent GetPreviewTitle ()
		{
			return new GUIContent ("Preview");
		}


		/// <summary>
		/// Raises the preview settings event.
		/// </summary>
		public override void OnPreviewSettings ()
		{
			GUIStyle preLabel  = new GUIStyle ("preLabel");
			GUIStyle preButton = new GUIStyle ("preButton");

			GUILayout.Label ("Label", preLabel);
			GUILayout.Button ("Button", preButton);
		}


		/// <summary>
		/// Raises the preview GU event.
		/// </summary>
		/// <param name="r">The red component.</param>
		/// <param name="background">Background.</param>
		public override void OnPreviewGUI (Rect r , GUIStyle background)
		{
			previewRenderUtility.BeginPreview (r, background);

			var previewCamera = previewRenderUtility.m_Camera;

			previewCamera.transform.position = previewObject.transform.position + new Vector3 (0f, 2.5f, -5f);

			previewCamera.transform.LookAt (previewObject.transform);
			previewCamera.Render ();

			previewRenderUtility.EndAndDrawPreview (r);
		}


	}



}

