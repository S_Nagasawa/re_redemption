﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Product;
using UniRx;
using UniRx.Triggers;
using System.Linq;


namespace Product 
{
	/// <summary>
	/// Example2.
	/// windowが開いて、右上のタブメニューの拡張
	/// </summary>
	public class Example2 : EditorWindow, IHasCustomMenu
	{

		static Example2 exampleWindow;
	
		/// <summary>
		/// Adds the item to menu.
		/// </summary>
		/// <param name="menu">Menu.</param>
		public void AddItemsToMenu (GenericMenu menu)
		{
			menu.AddItem (new GUIContent ("example"), false, () => 
				{
					Debug.Log("example");	
				}
			);

			menu.AddItem (new GUIContent ("example2"), true, () => 
				{
					Debug.Log("example2");
				}
			);
		}


		/// <summary>
		/// Open this instance.
		/// </summary>
		[MenuItem("Redemption/Study/Example2")]
		static void Open ()
		{
			if (exampleWindow == null)
			{
				exampleWindow = Example2.GetWindow<Example2>();
			}
		}


		/// <summary>
		/// Validates the example2.
		/// Windowが開かないように処理
		/// </summary>
		/// <returns><c>true</c>, if example2 was validated, <c>false</c> otherwise.</returns>
		/*
		[MenuItem("Redemption/Study/Example2", true)]
		static bool ValidateExample2 ()
		{
			return false;
		}
		*/


		/// <summary>
		/// Raises the GU event.
		/// </summary>
		void OnGUI ()
		{
			//_EditorWindowShowLog ();
		}


		/// <summary>
		/// Editors the window show log.
		/// </summary>
		void _EditorWindowShowLog ()
		{
			var sceneView = Resources.FindObjectsOfTypeAll<EditorWindow>();

			foreach (var s in sceneView) 
			{
				Debug.Log (s.GetType().ToString());
			}
		}


	}
}

