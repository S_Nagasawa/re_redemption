﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Product;
using UniRx;
using UniRx.Triggers;
using System.Linq;


namespace Product.Example
{
	/// <summary>
	/// Exsample.
	/// </summary>
	public class Example : EditorWindow
	{

		#region static
		static Example exampleWindow;
		#endregion

		#region private
		private ExamplePopupContent m_PopupContent = new ExamplePopupContent();
		[SerializeField] private float m_MainButtonWidth  = 128f;
		[SerializeField] private float m_MainButtonHeight = 44f;
		#endregion

		/// <summary>
		/// Open this instance.
		/// </summary>
		[MenuItem("Redemption/Study/Example")]
		static void Open ()
		{
			if (exampleWindow == null)
			{
				exampleWindow = Example.GetWindow<Example>();
			}
			exampleWindow.ShowUtility ();
			//DisplayWizard<Exsample> ("Example Window");
		}
		Rect node1 = new Rect(10f, 10f, 100f, 100f);
		Rect node2 = new Rect(10f, 10f, 100f, 100f);


		/// <summary>
		/// Raises the GU event.
		/// </summary>
		void OnGUI ()
		{
			BeginWindows ();
			/*
			EditorGUILayout.Space ();
			_CreatePopupWindow ();
			EditorGUILayout.Space ();
			_CreateNodeChilds ();
			EditorGUILayout.Space ();
			_ExampleView ();
			*/
			_OnExampleScrollView ();
			EndWindows ();
		}


		int leftSize = 10;
		Vector2 leftScrollPos = Vector2.zero;
		int rightSize = 10;
		Vector2 rightScrollPos = Vector2.zero;
		/// <summary>
		/// Ons the example scroll view.
		/// </summary>
		void _OnExampleScrollView ()
		{
			EditorGUILayout.LabelField ("ScrollView Sample");

			//start horizontal
			EditorGUILayout.BeginHorizontal (GUI.skin.box);
			{
				//left
				EditorGUILayout.BeginVertical (GUI.skin.box, GUILayout.Width(300));
				EditorGUILayout.LabelField ("Left");

				leftSize = EditorGUILayout.IntSlider ( "Size",leftSize, 10, 100,GUILayout.ExpandWidth(false) );

				// 左側のスクロールビュー(横幅300px)
				leftScrollPos = EditorGUILayout.BeginScrollView( leftScrollPos,GUI.skin.box );
				{
					// スクロール範囲
					for( int i = 0; i < leftSize; ++i )
					{
						EditorGUILayout.LabelField( "Index " + i );
					}
				}
				EditorGUILayout.EndScrollView();
				EditorGUILayout.EndVertical();
			}
			{
				//right
				EditorGUILayout.BeginVertical (GUI.skin.box, GUILayout.Width((Screen.width/2) - 10));
				EditorGUILayout.LabelField ("Right");

				rightSize = EditorGUILayout.IntSlider ( "Size",rightSize, 10, 100,GUILayout.ExpandWidth(false) );
				rightScrollPos = EditorGUILayout.BeginScrollView (rightScrollPos, GUI.skin.box);
				{
					for (int y = 0; y < rightSize; ++y) 
					{
						EditorGUILayout.BeginHorizontal (GUI.skin.box);
						{
							EditorGUILayout.PrefixLabel ("Index" + y);
							if (GUILayout.Button ("Button" + y, GUILayout.Width (100))) 
							{
								Debug.Log ("Button" + y + "Pressed");
							}
							/*
							for (int i = 0; i < y; ++i) 
							{
								if (GUILayout.Button ("Button" + i, GUILayout.Width (100))) 
								{
									Debug.Log ("Button" + i + "Pressed");
								}
							}
							*/
						}
						EditorGUILayout.EndHorizontal ();
					}
				}
				EditorGUILayout.EndScrollView ();
				EditorGUILayout.EndVertical ();
			}
			//end
			EditorGUILayout.EndHorizontal ();
		}



		/// <summary>
		/// Examples the view.
		/// </summary>
		bool toggle = false;
		string textField = "";
		string textArea = "";
		string password = "";
		float horizontalScrollbar = 0.0f;
		float verticalScrollbar = 0.0f;
		float horizontalSlider = 0.0f;
		float verticalSlider = 0.0f;
		int toolbar = 0;
		int selectionGrid = 0;
		void _ExampleView ()
		{
			EditorGUILayout.LabelField( "ようこそ！　Unityエディタ拡張の沼へ！" );
			GUILayout.Label( "Label : GUILayoutはUnityEngine側なので、ランタイムでもそのまま使える系" );

			if (GUILayout.Button ("Button")) 
			{
				Debug.Log( "Button!" );
			}

			if (GUILayout.RepeatButton ("RepeatButton")) 
			{
				Debug.Log( "RepeatButton!" );
			}

			toggle = GUILayout.Toggle( toggle,"Toggle" );

			GUILayout.Label ("TextArea");
			textArea = GUILayout.TextArea (textArea);
			GUILayout.Label( "PasswordField" );
			password = GUILayout.PasswordField( password,'*' );

			GUILayout.Label( "HorizontalScrollbar" );
			float horizontalSize = 10.0f;// sizeはバーのサイズ(0～100のスクロールバーで10なので、全体に対して10分の1サイズ)
			horizontalScrollbar = GUILayout.HorizontalScrollbar( horizontalScrollbar,horizontalSize,0.0f,100.0f );

			GUILayout.Label( "VerticalScrollbar" );
			float verticalSize = 10.0f;// sizeはバーのサイズ(0～100のスクロールバーで10なので、全体に対して10分の1サイズ)
			verticalScrollbar = GUILayout.VerticalScrollbar( verticalScrollbar,verticalSize,0.0f,100.0f );

			GUILayout.Label( "HorizontalSlider" );
			horizontalSlider = GUILayout.HorizontalSlider( horizontalSlider,0.0f,100.0f );

			GUILayout.Label( "VerticalSlider" );
			verticalSlider = GUILayout.VerticalSlider( verticalSlider,0.0f,100.0f );

			//tab
			GUILayout.Label( "TabBar" );
			toolbar = GUILayout.Toolbar( toolbar, new string[]
				{
					"Tab1",
					"Tab2",
					"Tab3" 
				}
			);

			GUILayout.Label( "SelectionGrid" );
			selectionGrid = GUILayout.SelectionGrid( selectionGrid, new string[]
				{ 
					"Grid 1",
					"Grid 2",
					"Grid 3",
					"Grid 4" 
				},2//横に生成するサイズ
			);

			GUILayout.Box( "Box" );

			GUILayout.Label( "ここからSpace" );
			GUILayout.Space(100);
			GUILayout.Label( "ここまでSpace" );

			GUILayout.Label( "ここからFlexibleSpace" );
			GUILayout.FlexibleSpace();
			GUILayout.Label( "ここまでFlexibleSpace" );
		}


		/// <summary>
		/// Creates the popup window.
		/// </summary>
		void _CreatePopupWindow ()
		{
			if (GUILayout.Button ("PopupContentButton", GUILayout.Width (m_MainButtonWidth), GUILayout.Height(m_MainButtonHeight)))
			{
				Rect activorRect = GUILayoutUtility.GetLastRect ();
				PopupWindow.Show (activorRect, m_PopupContent);
			}
		}


		/// <summary>
		/// Creates the node childs.
		/// </summary>
		void _CreateNodeChilds ()
		{
			node1 = GUI.Window(1, node1, WindowCallback, "node1", "flow node 2");
			node2 = GUI.Window(2, node2, WindowCallback, "node2", "flow node 1");
			var start = new Vector3(node1.x + node1.width, node1.y + node1.height / 2f, 0f);
			var startTan = start + new Vector3(100f, 0f, 0f);
			var end = new Vector3(node2.x, node2.y + node2.height / 2f, 0f);
			var endTan = end + new Vector3(-100f, 0f, 0f);
			Handles.DrawBezier(start, end, startTan, endTan, Color.gray, null, 3f);
		}


		/// <summary>
		/// Windows the callback.
		/// </summary>
		/// <param name="id">Identifier.</param>
		void WindowCallback (int id)
		{
			//Debug.Log (id);
			GUI.DragWindow ();
		}
	}


	/// <summary>
	/// Example popup content.
	/// </summary>
	public class ExamplePopupContent : PopupWindowContent
	{


		/// <summary>
		/// Raises the GU event.
		/// </summary>
		/// <param name="rect">Rect.</param>
		public override void OnGUI (Rect rect)
		{
			EditorGUILayout.LabelField ("Label");
		}


		/// <summary>
		/// Raises the open event.
		/// </summary>
		public override void OnOpen ()
		{
			//Debug.Log ("Open");
		}


		/// <summary>
		/// Raises the close event.
		/// </summary>
		public override void OnClose ()
		{
			//Debug.Log ("Close");
		}


		/// <summary>
		/// Gets the size of the window.
		/// </summary>
		/// <returns>The window size.</returns>
		public override Vector2 GetWindowSize ()
		{
			return new Vector2 (300f, 100f);
		}
	}

}

