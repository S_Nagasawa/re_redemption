﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Product.Tools
{
	/// <summary>
	/// Owner.
	/// </summary>
	public class Owner : MonoBehaviour
	{
		public enum CloneMode
		{
			/// <summary>
			/// The follow parent.
			/// </summary>
			FollowParent,
			/// <summary>
			/// The keep rotate.
			/// </summary>
			KeepRotate,
		}

		/// <summary>
		/// Prefab生成してクラスを返却
		/// </summary>
		/// <param name="go">Go.</param>
		/// <param name="position">Position.</param>
		/// <param name="parent">Parent.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		static public T Generate <T>(GameObject go, Vector3 position, Transform parent) where T : Component
		{
			var _obj = (GameObject)Instantiate(go, position, Quaternion.identity);
			if (parent != null) 
			{
				_obj.transform.SetParent (parent);
			}
			return _obj.GetComponent<T>() as T;
		}


		/// <summary>
		/// Resource Loadしてクラスを返却
		/// </summary>
		/// <returns>The resource.</returns>
		/// <param name="resourcePath">Resource path.</param>
		/// <param name="position">Position.</param>
		/// <param name="parent">Parent.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		static public T GetResource <T>(string resourcePath, Vector3 position, Transform parent) where T : Component
		{
			var _obj = (GameObject)Instantiate(Resources.Load(resourcePath), position, Quaternion.identity);
			if (parent != null) 
			{
				_obj.transform.SetParent (parent);
			}
			return _obj.GetComponent<T>() as T;
		}


		/// <summary>
		/// Clone the specified pPrefab, pParent and mode.
		/// </summary>
		/// <param name="pPrefab">P prefab.</param>
		/// <param name="pParent">P parent.</param>
		/// <param name="mode">Mode.</param>
		public static GameObject Clone (GameObject pPrefab, Transform pParent, CloneMode mode = CloneMode.FollowParent)
		{
			Transform rotationOrigin;
			switch (mode) 
			{
				case CloneMode.KeepRotate:
				rotationOrigin = pPrefab.transform;
				break;
				default:
				// Follow Parent Transform
				rotationOrigin = pParent;
				break;
			}
			return Instantiate (pPrefab, pParent.position, rotationOrigin.rotation) as GameObject;			
		}


		/// <summary>
		/// Clones as children.
		/// 引数の parent の子要素としてアタッチ
		/// </summary>
		/// <returns>The as children.</returns>
		/// <param name="prefab">Prefab.</param>
		/// <param name="parent">Parent.</param>
		/// <param name="mode">Mode.</param>
		public static GameObject CloneAsChildren (GameObject prefab, Transform parent, CloneMode mode = CloneMode.FollowParent) 
		{
			var _instance = Clone (prefab, parent, mode);
			_instance.transform.parent = parent;
			return _instance;
		}
			

		/// <summary>
		/// Clones as sibling.
		/// 引数の sibling の兄弟要素としてアタッチ
		/// </summary>
		/// <returns>The as sibling.</returns>
		/// <param name="prefab">Prefab.</param>
		/// <param name="sibling">Sibling.</param>
		/// <param name="mode">Mode.</param>
		public static GameObject CloneAsSibling (GameObject prefab, Transform sibling, CloneMode mode = CloneMode.FollowParent) 
		{
			var _instance = Clone (prefab, sibling, mode);
			_instance.transform.parent = sibling.parent;
			return _instance;
		}


	}




}
