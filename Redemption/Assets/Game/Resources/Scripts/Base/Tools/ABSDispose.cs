﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Product.Tools
{

	/// <summary>
	/// ABS dispose.
	/// </summary>
	public abstract class ABSDispose : UnityEngine.Object, System.IDisposable
	{
		/// <summary>
		/// Gets a value indicating whether this instance is disposed.
		/// </summary>
		/// <value><c>true</c> if this instance is disposed; otherwise, <c>false</c>.</value>
		public bool IsDisposed {
			get { 
				return m_IsDisposed;
			}
		}

		private bool m_IsDisposed = false;


		/// <summary>
		/// Releases all resource used by the <see cref="Product.ABSDispose"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="Product.ABSDispose"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="Product.ABSDispose"/> in an unusable state. After calling
		/// <see cref="Dispose"/>, you must release all references to the <see cref="Product.ABSDispose"/> so the garbage
		/// collector can reclaim the memory that the <see cref="Product.ABSDispose"/> was occupying.</remarks>
		public void Dispose ()
		{
			this.DoDispose (true);
			System.GC.SuppressFinalize (true);
		}


		/// <summary>
		/// Raises the dispose event.
		/// </summary>
		protected virtual void OnDispose () {}


		/// <summary>
		/// Raises the dispose event.
		/// </summary>
		/// <param name="flag">If set to <c>true</c> flag.</param>
		private void DoDispose (bool flag) 
		{
			if (!m_IsDisposed)
				OnDispose ();
			m_IsDisposed = true;
		}

		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the <see cref="Product.ABSDispose"/> is
		/// reclaimed by garbage collection.
		/// </summary>
		~ABSDispose ()
		{
			this.DoDispose (false);
		}
	}
}

