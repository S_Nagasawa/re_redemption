using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Coroutine base.
/// コルーチンに関する汎用クラス
/// </summary>
namespace Product
{
	namespace Tools
	{
		public static class CoroutineBase
		{

			public static readonly MonoBehaviour m_MonoBehaviour;

			/// <summary>
			/// コルーチンを管理するゲームオブジェクトを生成するコンストラクタ
			/// </summary>
			static CoroutineBase ()
			{
				var gameObject 	= new GameObject("CoroutineBase");		
				GameObject.DontDestroyOnLoad(gameObject);
				m_MonoBehaviour = gameObject.AddComponent<MonoBehaviour>();
			}

			/// <summary>
			/// Calls the wait for one frame.
			/// CoroutineBase.CallWaitForOneFrame ( () => Console.WriteLine("Attack"); });
			/// </summary>
			/// <param name="args">Arguments.</param>
			public static void CallWaitForOneFrame (Action args)
			{
				m_MonoBehaviour.StartCoroutine(DoCallWaitForOneFrame(args));
			}

			/// <summary>
			/// Calls the wait for seconds.
			/// CoroutineBase.CallWaitForSeconds (1f, () => Console.WriteLine("Attack"); });
			/// </summary>
			/// <param name="seconds">Seconds.</param>
			/// <param name="args">Arguments.</param>
			public static void CallWaitForSeconds (float seconds, Action args)
			{
				m_MonoBehaviour.StartCoroutine(DoCallWaitForSeconds(seconds, args));
			}

			/// <summary>
			/// 1 フレーム待機してから Action デリゲートを呼び出す
			/// </summary>
			private static IEnumerator DoCallWaitForOneFrame(Action args)
			{
				yield return 0;
				args();
			}

			/// <summary>
			/// 指定された秒数待機してから Action デリゲートを呼び出す
			/// </summary>
			private static IEnumerator DoCallWaitForSeconds(float seconds, Action args)
			{
				yield return new WaitForSeconds(seconds);
				args();
			}
		}

	}
}


