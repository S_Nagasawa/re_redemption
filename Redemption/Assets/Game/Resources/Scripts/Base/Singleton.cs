﻿using UnityEngine;
using System;
using System.Collections;


/// <summary>
/// Singleton.
/// </summary>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

	static T instance;

	/// <summary>
	/// Get the instance.
	/// </summary>
	/// <value>The instance.</value>
	public static T Instance
	{
		get {
			if (applicationIsQuitting) 
			{
				return null;
			}

			if (instance == null) 
			{
				instance = (T)FindObjectOfType (typeof(T));
				if(instance == null) 
				{
					GameObject go 	= new GameObject(typeof(T).ToString());
					instance 		= go.AddComponent<T>();
					DontDestroyOnLoad (go);
				}
			}
			return instance;
		}
	}

	public static bool applicationIsQuitting = false;

	public Transform TransformCache;

	public void OnDestroy ()
	{
		applicationIsQuitting = true;
	}
		
	protected virtual void OnDrawGizmos () {}

	/// <summary>
	/// Awake this instance.
	/// </summary>
	protected virtual void Awake ()
	{
		TransformCache = transform;
	}


}
