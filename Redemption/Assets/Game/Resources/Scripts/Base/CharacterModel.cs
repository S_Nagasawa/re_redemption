﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Product;
using System.Linq;
using UniRx;
using UniRx.Triggers;

//prod
using Product.Systems;

namespace Product.Character
{
	/// <summary>
	/// Character model.
	/// </summary>
	[RequireComponent(typeof(Rigidbody), typeof(Animator))]
	public class CharacterModel : ABSCharacter
	{
		#region AnimatorState
		public static readonly string OnForward     = "Forward";   //走る
		public static readonly string OnJump        = "Jump";      //jump
		public static readonly string OnTurn        = "Turn";      //turn
		public static readonly string HasGrounded   = "Grounded";  //着地したか
		public static readonly string OnGround      = "OnGround";  //着地
		public static readonly string OnJumpLeg     = "JumpLeg";   //ジャンプ中の下半身
		public static readonly string OnTargeting   = "Targeting"; //ターゲッティング
		public static readonly string OnShot        = "Shot";      //Shoot
		#endregion


        //[Range(1f, 4f),HideInInspector] public float GravityMultiplier = 2f;
        [HideInInspector] public float GravityMultiplier   = 2f;
		[HideInInspector] public float MovingTurnSpeed     = 360f;
		[HideInInspector] public float StationaryTurnSpeed = 180f;

        public float JumpPower              = 14f;
		public float RunCycleLegOffset      = 0.2f;
		public float MoveSpeedMultiplier    = 1f;
		public float AnimSpeedMultiplier    = 1f;
		public float GroundCheckDistance    = 0.3f;
		public float CircleHalf             = 0.5f;
		public Vector3 GroundNormal { get; set; }
		public bool IsGrounded      { get; set; } //接地
		public bool IsCrouching     { get; set; } //しゃがむ
		public bool IsSneaking      { get; set; } //Sneak
		public bool IsRaid          { get; set; } //奇襲

		[HideInInspector] public int LayerMask = 1 << 8;//256

		protected IObservable<long> LifeCycleStream;
		protected IObservable<long> TargetRangeStream;

		#region Static
		/// <summary>
		/// The follow character list.
		/// </summary>
		public static List<CharacterModel> CharacterModelList = new List<CharacterModel>();

		protected virtual void OnEnable ()
		{
			CharacterModelList.Add(this);
		}

		protected virtual void OnDisable ()
		{
			CharacterModelList.Remove(this);
		}
		#endregion


		/// <summary>
		/// Awake this instance.
		/// </summary>
		protected virtual void Awake ()
		{
			AnimatorController  = GetComponent<Animator> () as Animator;
			RigidController	    = GetComponent<Rigidbody> () as Rigidbody;
			CapsuleController   = GetComponent<CapsuleCollider> () as CapsuleCollider;
			NavMeshController   = GetComponent<NavMeshAgent> () as NavMeshAgent;
			WeaponCtrl          = GetComponent<WeaponController> () as WeaponController;
			LayerMask           = ~LayerMask;
			TransformCache      = transform;

		}


		/// <summary>
		/// Start this instance.
		/// </summary>
		protected virtual void Start () 
		{
			Debug.LogWarning (string.Format("Name:{0, 1}", Status.Name));
			Debug.LogWarning (string.Format("InstanceID:{0, 1}", this.GetInstanceID()));
			RigidController.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			//SomeClass CreateInstances
			{
				WeaponCtrl.CreateBulletRoot ();
				RangeManager           = new CharacterRange ();
				RangeManager.BaseRange = CharacterRange;
			}

			LifeCycleStream   = Observable.EveryUpdate ();
			TargetRangeStream = Observable.EveryUpdate ();

			/*
			CharacterModel.CharacterModelList.ForEach ((CharacterModel arg) => 
				{
					Debug.Log(arg);
				}
			);

			LifeCycleStream
				.Subscribe (_ => {
					if (Status != null && Status.IsDie ()) {
						Kill ();
					} else {
						DoUpdate ();
					}
				}).AddTo (this);
			*/

			TargetRangeStream
				.Where (_ => Target != null)
				.Subscribe (_ => {
					TargetRange = GetTargetRange();
				}).AddTo (this);
		}

		/// <summary>
		/// Initialize this instance.
		/// </summary>
		protected virtual IEnumerator Initialize ()
		{
			yield break;
		}

		/*
		protected virtual void Update () {}
		protected virtual void FixedUpdate () {}
		*/

		/// <summary>
		/// Do the update.
		/// </summary>
		protected virtual void DoUpdate () {}

		/// <summary>
		/// Move the specified move, crouch and jump.
		/// </summary>
		/// <param name="move">Move.</param>
		/// <param name="crouch">If set to <c>true</c> crouch.</param>
		/// <param name="jump">If set to <c>true</c> jump.</param>
		public virtual void Move (Vector3 move, bool crouch = false, bool jump = false) {}

		/// <summary>
		/// Shot this instance.
		/// </summary>
		public virtual void Shot () 
		{
			WeaponCtrl.Shot ();
		}
			
		/// <summary>
		/// Kill this instance.
		/// </summary>
		protected virtual void Kill () {}

		/// <summary>
		/// Updates the state.
		/// Playerはoverride
		/// </summary>
		/// <param name="move">Move.</param>
		protected virtual void UpdateState (Vector3? move = null)
		{
			var position = move ?? Vector3.zero;
			if (!IsGrounded) 
			{
				MovePattern = DefaultPattern.JUMP;
			}
			else
			{
				if (IsCrouching && position.magnitude <= 0) 
				{
					MovePattern = DefaultPattern.CROUCH;
				}
				else
				{
					if (position.magnitude > 0) 
					{
						MovePattern = DefaultPattern.MOVE;
					}
					if (position.magnitude <= 0) 
					{
						MovePattern = DefaultPattern.IDLE;						
					}
				}
			}
		}

		/// <summary>
		/// Updates the state action.
		/// </summary>
		/// <param name="move">Move.</param>
		protected virtual void UpdateStateAction (Vector3? move = null)
		{
			var position = move ?? Vector3.zero;
			if (AnimatorController.GetBool (CharacterModel.OnTargeting)) 
			{
				AttackPattern = ActionPattern.GUN_MOTION;
			}
			else
			{
				AttackPattern = ActionPattern.NONE;
			}
		}

		/// <summary>
		/// Checks the ground status.
		/// Jumping At the grounding.
		/// </summary>
		protected virtual void CheckGroundStatus()
		{
			RaycastHit hitInfo;
			#if UNITY_EDITOR
			//Debug.DrawLine(TransformCache.position + (Vector3.up * 0.1f), TransformCache.position + (Vector3.up * 0.1f) + (Vector3.down * GroundCheckDistance), Color.blue);
			#endif
			if (Physics.Raycast(TransformCache.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, GroundCheckDistance))
			{
				IsGrounded   = true;
				GroundNormal = hitInfo.normal;
				AnimatorController.applyRootMotion = true;
			}
			else
			{
				IsGrounded   = false;
				GroundNormal = Vector3.up;
				AnimatorController.applyRootMotion = false;
			}
		}
			
		/// <summary>
		/// Raises the animator move event.
		/// Anmator override to override
		/// </summary>
		public virtual void OnAnimatorMove ()
		{
			if (IsGrounded && Time.deltaTime > 0)
			{
				Vector3 v = (AnimatorController.deltaPosition * MoveSpeedMultiplier) / Time.deltaTime;
				v.y       = RigidController.velocity.y;
				RigidController.velocity = v;
			}
		}

		/// <summary>
		/// Raises the animator IK event.
		/// Params
		/// weight	(0-1) LookAt のグローバルウェイト、他のパラメーターの係数
		/// bodyWeight	(0-1) ボディが LookAt にどれぐらい関係するか決定
		/// headWeight	(0-1) 頭 (head)が LookAt にどれぐらい関係するか決定
		/// eyesWeight	(0-1) 目が LookAt にどれぐらい関係するか決定
		/// clampWeight	(0-1) 0.0 の場合、キャラクターはモーションの制限が 0 であり、1.0 の場合、
		/// キャラクターは完全に制限され（ LookAt が不可能）、0.5 の場合、使用可能な範囲の半分（ 180 度）まで移動
		/// </summary>
		public virtual void OnAnimatorIK () {}

		/// <summary>
		/// Updates the animator.
		/// </summary>
		/// <param name="move">Move.</param>
		protected virtual void UpdateAnimator(Vector3? move = null) {}

		/// <summary>
		/// Applies the extra turn rotation.
		/// </summary>
		protected virtual void ApplyExtraTurnRotation(float forwardAmount, float turnAmount)
		{
			float turnSpeed = Mathf.Lerp(StationaryTurnSpeed, MovingTurnSpeed, forwardAmount);
			TransformCache.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
		}
			
		/// <summary>
		/// Handles the grounded movement.
		/// </summary>
		/// <param name="crouch">If set to <c>true</c> crouch.</param>
		/// <param name="jump">If set to <c>true</c> jump.</param>
		protected virtual void HandleGroundedMovement(bool crouch = false, bool jump = false)
		{
			if (jump && !crouch && AnimatorController.GetCurrentAnimatorStateInfo(0).IsName(CharacterModel.HasGrounded))
			{
				RigidController.velocity            = new Vector3(RigidController.velocity.x, JumpPower, RigidController.velocity.z);
				IsGrounded                          = false;
				AnimatorController.applyRootMotion  = false;
				GroundCheckDistance                 = 0.1f;
			}
		}
			
		/// <summary>
		/// Handles the airborne movement.
		/// </summary>
		/// <param name="originalDistance">Original distance.</param>
		protected virtual void HandleAirborneMovement(float originalDistance)
		{
			Vector3 extraGravityForce = (Physics.gravity * GravityMultiplier) - Physics.gravity;
			RigidController.AddForce(extraGravityForce);
			GroundCheckDistance = RigidController.velocity.y < 0 ? originalDistance : 0.01f;
		}
			
		/// <summary>
		/// Determines whether this instance has target.
		/// </summary>
		/// <returns><c>true</c> if this instance has target; otherwise, <c>false</c>.</returns>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		protected virtual bool HasTarget<T> () where T : Component
		{
			return (Target != null && Target.gameObject.HasComponent<T> ());
		}

		protected virtual void UpdateWayPoint (Vector3? move = null) {}
		protected virtual void UpdateShooting (Vector3? move = null) {}
		protected virtual void UpdateCovering (Vector3? move = null) {}
		protected virtual void UpdateEscaping (Vector3? move = null) {}
		protected virtual void UpdateReset    (Vector3? move = null) {}
		protected virtual void ArrivalAtWayPoint () {}

		/// <summary>
		/// Raises the wait event.
		/// </summary>
		/// <param name="delta">Delta.</param>
		/// <param name="pOnStart">P onStart.</param>
		/// <param name="pOnEnd">P onEnd.</param>
		public virtual IEnumerator OnWait (float delta = 2f, System.Action pOnStart = null, System.Action pOnEnd = null) 
		{
			if (pOnStart != null)
			{ 
				pOnStart (); 
			}
			yield return null;

			var _action_time    = delta;
			var _time           = 0f;
			while (_time < _action_time) 
			{
				_time += Time.deltaTime;
				yield return null;
			}
			if (pOnEnd != null) 
			{
				pOnEnd (); 
			}
			yield return null;
		}

		/// <summary>
		/// Creates the awareness.
		/// </summary>
		protected virtual void CreateAwareness ()
		{
			if (AwarenessController == null) 
			{
				AwarenessController = Skin.GetComponent<Awareness> () as Awareness;
			}
			AwarenessController.Model   = this;
			AwarenessController.Agent   = NavMeshController;
		}

		#region WayPointSystem Handle Method
		/// <summary>
		/// The system.
		/// </summary>
		WayPointSystem system;
		/// <summary>
		/// Gets the state.
		/// </summary>
		/// <returns>The state.</returns>
		protected virtual AI_STATE GetAIState (object params_ = null, bool hasShooting = false)
		{
			AI_STATE _state = AI_STATE.NONE;

			if (params_ is WayPointSystem) 
			{
				system = (WayPointSystem)params_;
			}

			//何かしらの理由でWayPointがない
			if (system == null) 
			{
				return _state;
			}
			//他所で戦闘中か
			if (hasShooting) 
			{
				_state = AI_STATE.COVER;
			}
			//まだ戦闘していない
			else
			{
				//WayPointがない
				if (system.CanUseSystem()) 
				{
					//視野内
					if (AwarenessController.IsBothCapture()) 
					{
						_state = AI_STATE.SHOOTING;
					}
					//何れかがfalseの場合
					if (!AwarenessController.IsCharacterSighting() || !AwarenessController.IsTargetCatches())
					{
						_state = AI_STATE.NONE;
					}
				}
				//WayPointがある
				else if (!system.CanUseSystem())
				{
					//視野内
					if (AwarenessController.IsBothCapture()) 
					{
						_state = AI_STATE.SHOOTING;
					}
					//何れかがfalseの場合
					if (!AwarenessController.IsCharacterSighting() || !AwarenessController.IsTargetCatches())
					{
						_state = AI_STATE.PATROL;
					}
				}
			}
			return _state;
		}
		#endregion


		/// <summary>
		/// Gets the target range.
		/// </summary>
		/// <returns>The target range.</returns>
		protected virtual TARGET_RANGE GetTargetRange ()
		{
			TARGET_RANGE _range = TARGET_RANGE.NONE;

			float distance = (Target.position - TransformCache.position).magnitude;

			if (distance < RangeManager.LongRange && distance > RangeManager.MiddleRange) 
			{
				_range = TARGET_RANGE.LONG;
			}
			else if (distance < RangeManager.MiddleRange && distance > RangeManager.ShortRange)
			{
				_range = TARGET_RANGE.MIDDLE;
			}
			else if (distance < RangeManager.ShortRange && distance > 0f)
			{
				_range = TARGET_RANGE.SHORT;
			}
			return _range;
		}
	}



}


