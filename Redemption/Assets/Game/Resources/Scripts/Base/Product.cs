﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Product.Systems;

/// <summary>
/// Product namespace
/// </summary>
namespace Product 
{

	#region Character StatePattern
	/// <summary>
	/// Default pattern.
	/// キャラクターのState
	/// </summary>
	public enum DefaultPattern : byte
	{
		NONE,
		IDLE,
		MOVE,
		CROUCH,
		PICK_UP,
		TALK,
		JUMP,
	}

	/// <summary>
	/// Action pattern.
	/// 攻撃キャラクターのState
	/// </summary>
	public enum ActionPattern : byte
	{
		NONE,
		GUN_MOTION,
		SHOTGUN_MOTION,
		THROW_WEAPON_MOTION,
		KNIFE_MOTION,
		PICK_UP,
		PICK_DOWN,
		THROW,
	}

	/// <summary>
	/// AI Pattern
	/// </summary>
	public enum AI_STATE : byte
	{
		NONE,     //Wait
		PATROL,	  //巡回移動
		SHOOTING, //攻撃開始
		COVER,    //戦闘中の仲間にカバー
		ESCAPING, //逃走
		RAID,     //奇襲された
	}

	/// <summary>
	/// TARGET RANGE. 物の距離感
	/// </summary>
	public enum TARGET_RANGE : byte
	{
		NONE,
		LONG,
		MIDDLE,
		SHORT,
	}

	#endregion


	#region Abstract CharacterClass
	/// <summary>
	/// ABS character.
	/// キャラクタークラス
	/// </summary>
	public abstract class ABSCharacter : MonoBehaviour
	{
		/// <summary>
		/// Gets the status.
		/// </summary>
		/// <value>The status.</value>
		public CharacterStatus  Status 				{ get; set; }
		public Animator         AnimatorController 	{ get; set; }
		public CapsuleCollider  CapsuleController 	{ get; set; }
		public Rigidbody        RigidController 	{ get; set; }
		public NavMeshAgent     NavMeshController 	{ get; set; }
		public Awareness        AwarenessController { get; set; }
		public WeaponController WeaponCtrl          { get; set; }
		public Transform        TransformCache      { get; set; }
		public CharacterRange   RangeManager        { get; set; }
		public DefaultPattern   MovePattern;
		public ActionPattern    AttackPattern;
		public TARGET_RANGE     TargetRange;


		/// <summary>
		/// The target.
		/// </summary>
		public Transform Target;
		/// <summary>
		/// The skin.
		/// </summary>
		[Tooltip("キャラクターのメインメッシュ")] public Transform Skin;
		/// <summary>
		/// The character range.
		/// </summary>
		[Tooltip("キャラクターが行動を判断する基本的な距離")]public float CharacterRange;
	}
	#endregion


	#region ItemModel
	/// <summary>
	/// ABS item.
	/// </summary>
	public abstract class ABSItem : MonoBehaviour
	{
		public string Name;
		public string Source;
		public string Description;
		public Vector3 Position;
		public GameObject Prefab;
		public Transform TransformCache;

	
		/// <summary>
		/// Raises the disable event.
		/// </summary>
		public void OnDisable() 
		{
			Destroy (TransformCache.gameObject);
		}
	}

	/// <summary>
	/// Item base.
	/// </summary>
	public class ItemBase : ABSItem
	{

		public ItemBase (string pName = "", string pSource = "", string pDescription = "", Vector3? pPosition = null)
		{
			Name 		= pName;
			Source 		= pSource;
			Description = pDescription;
			Position 	= pPosition?? Vector3.zero;
		}

		public static ItemBase GetInstance (string pName = "", string pSource = "", string pDescription = "", Vector3? pPosition = null)
		{
			return new ItemBase (pName, pSource, pDescription, pPosition);
		}


	}
	#endregion


	#region TagManager
	/// <summary>
	/// Tags.
	/// </summary>
	public class Tags
	{
		public static readonly string PLAYER = "Player"; //Player
		public static readonly string TROOP  = "Troop";  //Troop | EnemyAI
		public static readonly string MOB    = "Mob";    //mob
		public static readonly string SKIN   = "Skin";   //AwarenessControllerをgetするSkin * Skin mesh rendererがついているGameobject

		public static readonly string STAGE  = "Stage";  //Stage
		public static readonly string WALL   = "Wall";   //Wall
	}

	#endregion


	#region User Data
	[System.Serializable]
	public class UserData
	{
		public Vector3 Position {
			get { 
				return m_Position;
			}
			set { 
				m_Position = value;
			}
		}
		[SerializeField] private Vector3 m_Position;
		public string Name   { get; set;}
		public int Id        { get; set;}
	}
	#endregion


	/// <summary>
	/// Character range.
	/// </summary>
	[System.Serializable]
	public class CharacterRange
	{
		[SerializeField] private float m_BaseRange;
		public float BaseRange {
			get {
				return m_BaseRange;
			}
			set { 
				m_BaseRange = value;
			}
		}
		public float MiddleRange 
		{
			get { 
				return m_BaseRange;
			}
		}
		public float LongRange 
		{
			get { 
				return m_BaseRange * 2f;
			}
		}
		public float ShortRange
		{
			get { 
				return m_BaseRange * 0.5f;
			}
		}
	}


	/// <summary>
	/// Some class.
	/// </summary>
	[System.SerializableAttribute]
	[System.Runtime.InteropServices.ComVisibleAttribute(true)]
	public class PublishData : System.IDisposable
	{
		private System.IntPtr unmanagedResource;
		private System.IO.StreamWriter managedResource;


		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the <see cref="Product.PublishData"/> is
		/// reclaimed by garbage collection.
		/// </summary>
		~PublishData()
		{
			this.Dispose(false);
		}


		/// <summary>
		/// Releases all resource used by the <see cref="Product.PublishData"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="Product.PublishData"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="Product.PublishData"/> in an unusable state. After calling
		/// <see cref="Dispose"/>, you must release all references to the <see cref="Product.PublishData"/> so the garbage
		/// collector can reclaim the memory that the <see cref="Product.PublishData"/> was occupying.</remarks>
		public void Dispose()
		{
			this.Dispose(true);
		}


		/// <summary>
		/// Dispose the specified isDisposing.
		/// </summary>
		/// <param name="isDisposing">If set to <c>true</c> is disposing.</param>
		private void Dispose(bool isDisposing)
		{
			this.Free(this.unmanagedResource);

			if (isDisposing)
			{
				if (this.managedResource != null)
				{
					this.managedResource = null;
				}
			}
		}


		/// <summary>
		/// Free the specified unmanagedResource.
		/// </summary>
		/// <param name="unmanagedResource">Unmanaged resource.</param>
		private void Free(System.IntPtr unmanagedResource)
		{
			// 省略
		}
	}



}


