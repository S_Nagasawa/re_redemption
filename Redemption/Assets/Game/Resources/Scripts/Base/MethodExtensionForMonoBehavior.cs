﻿using UnityEngine;
using UniRx;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


//色々ツールシングルトンクラス
public static class MethodExtensionForMonoBehaviour
{
	/// <summary>
	/// Removes the name of the clone.
	/// </summary>
	/// <param name="self">Self.</param>
	public static void RemoveCloneName (this GameObject self)
	{
		string name = self.name;
		if(name.IndexOf("(Clone)") > 0)
		{
			name = name.Replace("(Clone)","");
		}
	}


	/// <summary>
	/// Res the name of the name clone.
	/// </summary>
	/// <param name="self">Self.</param>
	/// <param name="name">Name.</param>
	public static void ReNameCloneName (this GameObject self, string name)
	{
		self.name = name;
	}


	/// <summary>
	/// Determines if is null or white space the specified self.
	/// nullチェック。空白文字も対象外に
	/// Usage str.IsNullOrWhiteSpace()
	/// </summary>
	/// <returns><c>true</c> if is null or white space the specified self; otherwise, <c>false</c>.</returns>
	/// <param name="self">Self.</param>
	public static bool IsNullOrWhiteSpace (this string self)
	{
		return self == null || self.Trim () == "";
	}

	/// <summary>
	/// Determines if is null or empty the specified self.
	/// nullチェック。list
	/// Usage list.IsNullOrEmpty()
	/// </summary>
	/// <returns><c>true</c> if is null or empty the specified self; otherwise, <c>false</c>.</returns>
	/// <param name="self">Self.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool IsNullOrEmpty<T> (this IList<T> self)
	{
		return self == null || self.Count == 0;
	}

	/// <summary>
	/// Safes the get component.
	/// 指定されたコンポーネントを返す
	/// Usage var player = gameObject.SafeGetComponent<Player>();
	/// </summary>
	/// <returns>The get component.</returns>
	/// <param name="self">Self.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T SafeGetComponent<T> (this GameObject self) where T : Component
	{
		return self.GetComponent<T>() ?? self.AddComponent<T>();
	}

	/// <summary>
	/// Gets the children.
	/// すべての子オブジェクトを返す
	/// foreach (var n in gameObject.GetChildren()) {}
	/// </summary>
	/// <returns>The children.</returns>
	/// <param name="self">Self.</param>
	/// <param name="includeInactive">If set to <c>true</c> include inactive.</param>
	public static GameObject[] GetChildren (this GameObject self, bool includeInactive = false)
	{
		return self
			.GetComponentsInChildren<Transform>( includeInactive )
			.Where( c => c != self.transform )
			.Select( c => c.gameObject )
			.ToArray();
	}

	/// <summary>
	/// Gets the children without grandchildren.
	/// 孫オブジェクトを除くすべての子オブジェクトを返す
	/// </summary>
	/// <returns>The children without grandchildren.</returns>
	/// <param name="self">Self.</param>
	public static GameObject[] GetChildrenWithoutGrandchildren (this GameObject self)
	{
		var result = new List<GameObject> ();
		foreach (Transform n in self.transform)
		{
			result.Add (n.gameObject);
		}
		return result.ToArray ();
	}

	/// <summary>
	/// Gets the components in children without self.
	/// 自分自身を除くすべての子オブジェクトにアタッチされている指定されたコンポーネントを返す
	/// Usage var uiWidgetList = gameObject.GetComponentsInChildrenWithoutSelf<UIWidget>();
	/// </summary>
	/// <returns>The components in children without self.</returns>
	/// <param name="self">Self.</param>
	/// <param name="includeInactive">If set to <c>true</c> include inactive.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T[] GetComponentsInChildrenWithoutSelf<T> (this GameObject self, bool includeInactive = false) where T : Component
	{
		return self
			.GetComponentsInChildren<T> (includeInactive)
			.Where (c => self != c.gameObject)
			.ToArray ();
	}

	/// <summary>
	/// Removes the component.
	/// 指定されたコンポーネントを削除
	/// Usage gameObject.RemoveComponent<Player>();
	/// </summary>
	/// <param name="self">Self.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void RemoveComponent<T> (this GameObject self) where T : Component
	{
		Object.Destroy (self.GetComponent<T>());
	}

	/// <summary>
	/// Removes the component immediate.
	/// 指定されたコンポーネントを即座に削除
	/// Usage gameObject.RemoveComponentImmediate<Player>();
	/// </summary>
	/// <param name="self">Self.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void RemoveComponentImmediate<T>( this GameObject self ) where T : Component
	{
		GameObject.DestroyImmediate( self.GetComponent<T>() );
	}

	/// <summary>
	/// Removes the components.
	/// 指定されたコンポーネントをすべて削除
	/// Usage gameObject.RemoveComponents<iTween>();
	/// </summary>
	/// <param name="self">Self.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void RemoveComponents<T> (this GameObject self) where T : Component
	{
		foreach (var n in self.GetComponents<T>())
		{
			GameObject.Destroy(n);
		}
	}

	/// <summary>
	/// Determines if has component the specified self.
	/// 指定されたコンポーネントがアタッチされているかを調べる
	/// Usage if ( gameObject.HasComponent<UISprite>() ) {}
	/// </summary>
	/// <returns><c>true</c> if has component the specified self; otherwise, <c>false</c>.</returns>
	/// <param name="self">Self.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool HasComponent<T> (this GameObject self) where T : Component
	{
		return self.GetComponent<T> () != null;
	}

	/// <summary>
	/// Find the specified self and name.
	/// 指定された名前で子オブジェクトを検索
	/// Usage var player = gameObject.transform.Find("Player");
	/// </summary>
	/// <param name="self">Self.</param>
	/// <param name="name">Name.</param>
	public static Transform Find (this GameObject self, string name)
	{
		return self.transform.Find (name);
	}

	/// <summary>
	/// Finds the game object.
	/// 指定された名前で子オブジェクトを検索して GameObject 型で返す
	/// Usage var player = gameObject.FindGameObject( "Player" );
	/// </summary>
	/// <returns>The game object.</returns>
	/// <param name="self">Self.</param>
	/// <param name="name">Name.</param>
	public static GameObject FindGameObject( this Component self,  string name )
	{
		var result     = self.transform.Find( name );
		return result != null ? result.gameObject : null;
	}

	/// <summary>
	/// Finds the component.
	/// 指定された名前で子オブジェクトを検索してその子オブジェクトから指定されたコンポーネントを取得
	/// Usage var player = gameObject.FindComponent<Player>( "Player" );
	/// </summary>
	/// <returns>The component.</returns>
	/// <param name="self">Self.</param>
	/// <param name="name">Name.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T FindComponent<T>( this Component self, string name ) where T : Component
	{
		var t = self.transform.Find( name );
		if ( t == null )
		{
			return null;
		}
		return t.GetComponent<T>();
	}

	/// <summary>
	/// Finds the deep.
	/// 指定された名前で子オブジェクトを深い階層まで検索して GameObject 型で返す
	/// Usage var player = transform.FindDeep( "Player" );
	/// </summary>
	/// <returns>The deep.</returns>
	/// <param name="self">Self.</param>
	/// <param name="name">Name.</param>
	/// <param name="includeInactive">If set to <c>true</c> include inactive.</param>
	public static GameObject FindDeep( this GameObject self, string name, bool includeInactive = false )
	{
		var children = self.GetComponentsInChildren<Transform>( includeInactive );
		foreach ( var transform in children )
		{
			if ( transform.name == name )
			{
				return transform.gameObject;
			}
		}
		return null;
	}

	/// <summary>
	/// Gets all children.
	/// 子オブジェクトからタグがマッチするTransformを返却
	/// Usage Transform tr = transform.GetTargetChildren("TagName");
	/// </summary>
	/// <param name="root">Root.</param>
	public static Transform GetTargetChildren (this Transform self, string tag)
	{
		Transform _tr = null;
		foreach (Transform n in self.gameObject.GetComponentsInChildren<Transform>() )
		{
			if (n.gameObject.CompareTag(tag))
			{
				_tr = n;
			}
		}
		return _tr;
	}

	/// <summary>
	/// Revisions the turn.
	/// </summary>
	/// <returns>The turn.</returns>
	/// <param name="self">Self.</param>
	/// <param name="flip">Flip.</param>
	public static Vector3 RevisionTurn (this Vector3 self, float flip)
	{
		Vector3 position = (flip > 0) ? self : new Vector3 (self.x = self.x * -1 , self.y, self.z);
		return position;
	}


	/// <summary>
	/// Sets the weapon transform.
	/// position, scale, rotateを一括更新 local
	/// usage transform.SetTotalTransform(Vector3, Vector3, Vector3);
	/// </summary>
	/// <param name="scale">Scale.</param>
	/// <param name="position">Position.</param>
	/// <param name="angles">Angles.</param>
	public static void SetTotalTransform (this Transform self, Vector3? scale = null, Vector3? position = null, Vector3? angles = null)
	{
		Vector3 _scale    = scale ?? Vector3.one;
		Vector3 _position = position ?? Vector3.zero;
		Vector3 _angles   = angles ?? Vector3.zero;
		self.localScale       = _scale;
		self.localPosition    = _position;
		self.localEulerAngles = _angles;
	}


}





